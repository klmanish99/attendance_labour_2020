<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
public function gettotal_bal($emp_code, $conn) {
	$sql = "SELECT `pl_bal`, `cl_bal`, `sl_bal`, `pl_acc`, `cl_acc`, `sl_acc`, `cof_acc`, leave_id FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
	$query = query($sql, $conn);
	return $query->row;
}
public function getacc_value($leave_id, $leave_type, $conn) {
	$sql = "SELECT `value` FROM `oc_leave_employee` WHERE `leave_id` = '".$leave_id."' AND `leavename` = '".$leave_type."' ";
	$query = query($sql, $conn);
	if($query->num_rows > 0){
		return $query->row['value'];
	} else {
		return 0;
	}
}
public function gettotal_bal_pro($emp_code, $type, $conn) {
	$prev_year = date('Y') - 1;
	$from_date = $prev_year.'-04-01';
	$to_date = $prev_year.'-03-31';
	$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND DATE(`date`) >= '".$from_date."' AND DATE(`date`) <= '".$to_date."' ";
	$query = query($sql, $conn);
	return $query->row;
}

public function gettotal_bal_pro1($emp_code, $type) {
	$prev_year = date('Y') - 1;
	$from_date = $prev_year.'-04-01';
	$to_date = $prev_year.'-03-31';
	$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND DATE(`date`) >= '".$from_date."' AND DATE(`date`) <= '".$to_date."' ";
	$query = query($sql, $conn);
	return $query->row;
}
if(date('Y-n-j') === '2018-04-18') {
	$employee_datas_sql = "SELECT  `emp_code` FROM `oc_employee` WHERE `company_id` <> '1' ";
	$employee_datas_sql = query($employee_datas_sql, $conn)->rows;
	foreach($employee_datas_sql as $ekey => $evalue){
		$from_dates = date('Y-m-26');
		$from_date = date('Y-m-d', strtotime($from_dates.' -1 month'));
		$to_date = date('Y-m-25');
		$prev_year = date('Y') - 1;
		$prev_leave_datas = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `year` >= '".$prev_year."' ", $conn)->rows;
		$total_bal = 0;
		if($prev_leave_datas->num_rows > 0){
			$prev_leave_data = $prev_leave_datas->row;
			$total_bal = gettotal_bal($evalue['emp_code'], $conn);
			if(isset($total_bal['leave_id'])){
				$acc_value = getacc_value($total_bal['leave_id'], 'PL', $conn);
				$total_bal_pro = gettotal_bal_pro($evalue['emp_code'], 'PL', $conn);
				$total_bal_pro1 = gettotal_bal_pro1($evalue'emp_code'], 'PL', $conn);
				$total_bal = 0;
				if($acc_value > 0){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal = $acc_value - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal = $acc_value;
					}
				}
			}
		}
		if($total_bal > '0'){
			$update_sql = "UPDATE `oc_leave` SET `pl_acc` = (`pl_acc` - ".$total_bal.") WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ";
			//query($update_sql, $conn);
		}
	}
}
$conn->close();
echo 'Done';exit;
?>