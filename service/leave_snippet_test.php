<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}

if(date('j') === '19') {
	$today_date = date('Y-m-d');
	$today_date = '2018-03-07';
	$compare_dates = date('Y-m-26');
	$compare_dates = '2018-03-26';
	$compare_date = date('Y-m-d', strtotime($compare_dates.' -2 month'));
	$employee_datas_sql = "SELECT  `emp_code`, `company`, `company_id`, `region`, `region_id`, `division`, `division_id`, `department`, `department_id`, `unit`, `unit_id`  FROM `oc_employee` WHERE `company_id` <> '1' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$compare_date."') AND `emp_code` = '50670022' ";
	//echo $employee_datas_sql;exit;
	$employee_datas_sql = query($employee_datas_sql, $conn)->rows;
	// echo '<pre>';
	// print_r($employee_datas_sql);
	// exit;
	$from_dates = date('Y-m-26');
	$from_dates = '2018-03-26';
	$from_date = date('Y-m-d', strtotime($from_dates.' -2 month'));
	$to_dates = date('Y-m-25');
	$to_dates = '2018-03-25';
	$to_date = date('Y-m-d', strtotime($to_dates.' -1 month'));

	$current_month = date('n', strtotime($to_date . ' +0 month'));
	if($current_month == 1){
		$current_year = date('Y', strtotime($to_date . ' +0 month'));
	} else {
		$current_year = date('Y', strtotime($to_date . ' +0 month'));
	}
	foreach($employee_datas_sql as $ekey => $evalue){
		$transaction_datas = query("SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$evalue['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' ", $conn)->rows;
		//echo "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$evalue['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' ";exit;
		$present_count = 0;
		$leave_count = 0;
		foreach($transaction_datas as $tkey => $tvalue){
			if($tvalue['leave_status'] == '1'){
				if($tvalue['firsthalf_status'] == 'OD'){
					$present_count = $present_count + 1;
				}
				if($tvalue['firsthalf_status'] == 'PL'){
					$leave_count = $leave_count + 1;
				}
			} elseif($tvalue['leave_status'] == '0.5'){
				if($tvalue['firsthalf_status'] == 'OD'){
					$present_count = $present_count + 0.5;
					if($tvalue['secondhalf_status'] == '1'){
						$present_count = $present_count + 0.5;
					}
				}
				if($tvalue['secondhalf_status'] == 'OD'){
					$present_count = $present_count + 0.5;
					if($tvalue['firsthalf_status'] == '1'){
						$present_count = $present_count + 0.5;
					}
				}
				if($tvalue['firsthalf_status'] == 'PL'){
					$leave_count = $leave_count + 0.5;
				}
				if($tvalue['secondhalf_status'] == 'PL'){
					$leave_count = $leave_count + 0.5;
				}
			} elseif($tvalue['weekly_off'] != '0'){
				$present_count = $present_count + 1;
			} elseif($tvalue['holiday_id'] != '0'){
				$present_count = $present_count + 1;
			} elseif($tvalue['present_status'] == '1'){
				$present_count = $present_count + 1;
			} elseif($tvalue['present_status'] == '0.5'){
				$present_count = $present_count + 0.5;
			}
		}
		
		// echo $evalue['emp_code'];
		// echo '<br />';
		// echo $present_count;
		// echo '<br />';
		// exit;

		if($present_count >= '9.5'){
			if($present_count >= '9.5' && $present_count <= '14.5'){
				$bal_to_add = 0.5;
			} elseif($present_count >= '15' && $present_count <= '24.5'){
				$bal_to_add = 1;	
			} else {
				$bal_to_add = 1.5;
			}

			// echo $bal_to_add;
			// echo '<br />';
			//exit;

			$is_exist_leaves = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
			if($is_exist_leaves->num_rows > 0){
				$is_exist_leave = $is_exist_leaves->row;
				$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
				if($is_exist_pls->num_rows > 0){
					$is_exist_pl = $is_exist_pls->row;
					$update_sql = "UPDATE `oc_leave_employee` SET `value` = (`value` + ".$bal_to_add.") WHERE `id` = '".$is_exist_pl['id']."' ";
				} else {
					$update_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$is_exist_leave['leave_id']."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."' ";
				}
				// echo $update_sql;
				// echo '<br />';
				//exit;
				//query($update_sql, $conn);
				
				$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `company` = '".$evalue['company']."', `company_id` = '".$evalue['company_id']."', `region` = '".$evalue['region']."', `region_id` = '".$evalue['region_id']."', `division` = '".$evalue['division']."', `division_id` = '".$evalue['division_id']."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."', `present_count` = '".$present_count."', `leave_taken` = '".$leave_count."' ";
				//query($insert_sql, $conn);
				echo $insert_sql;
				echo '<br />';
				echo '<br />';
				exit;
			}
		}
	}
	//echo 'Done';
	//exit;
}
$conn->close();
echo 'Done';exit;
?>