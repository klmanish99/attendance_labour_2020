<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
//if(date('j') === '7') {
	$compare_date = date('2018-02-26');
	$employee_datas_sql = "SELECT  `emp_code`, `company`, `company_id`, `region`, `region_id`, `division`, `division_id`, `department`, `department_id`, `unit`, `unit_id`, `name`, `doj` FROM `oc_employee` WHERE `company_id` <> '1' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$compare_date."') ";
	//echo $employee_datas_sql;exit;
	$employee_datas_sql = query($employee_datas_sql, $conn)->rows;
	// echo '<pre>';
	// print_r($employee_datas_sql);
	// exit;
	foreach($employee_datas_sql as $ekey => $evalue){
		$from_date = '2018-02-26';
		$to_date = '2018-03-25';
		$is_exist_leaves = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
		if($is_exist_leaves->num_rows > 0){
			$is_exist_leave = $is_exist_leaves->row;
			$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
			if($is_exist_pls->num_rows > 0){
				$is_exist_pl = $is_exist_pls->row;

				$update_old_leave_sql = "UPDATE `oc_leave` SET `close_status` = '1' WHERE `leave_id` = '".$is_exist_leave['leave_id']."' ";
				// echo $update_old_leave_sql;
				// echo '<br />';
				query($update_old_leave_sql, $conn);

				$db_value = $is_exist_pl['value'];
				$acc_value = $is_exist_pl['value'];
				$year = $is_exist_leave['year'];
				$year_1 = $is_exist_leave['year'] + 1;
				$comp_date = $year.'-03-26';
				$comp_date_end = $year_1.'-03-25';
				
				$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction` WHERE `emp_id` = '" . $evalue['emp_code'] . "' AND `leave_amount` = '' AND `leave_type`= 'PL' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' AND date(`date`) <= '".$comp_date_end."' ";
				// echo $sql;
				// echo '<br />';
				$leave_1_datas = query($sql, $conn);
				$bal_p = 0;
				if($leave_1_datas->num_rows > 0){
					$leave_1_data = $leave_1_datas->row;
					$bal_p = $leave_1_data['bal_p'];
					if($bal_p > 0){
					} else {
						$bal_p = 0;
					}
				}

				$comp_date = $year.'-03-26';
				$comp_date_end = $year_1.'-03-25';
				$sql1 = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $evalue['emp_code'] . "' AND `days` = '' AND `leave_type`= 'PL' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' AND date(`date`) <= '".$comp_date_end."' ";
				// echo $sql1;
				// echo '<br />';
				$leave_2_datas = query($sql1, $conn);
				$leave_amount = 0;
				if($leave_2_datas->num_rows > 0){
					$leave_2_data = $leave_2_datas->row;
					$leave_amount = $leave_2_data['leave_amount'];
					if($leave_amount > 0){
					} else {
						$leave_amount = 0;
					}
				}
				$total_bal = $acc_value - ($bal_p + $leave_amount);
				$db_value = $total_bal;
				
				// echo $acc_value;
				// echo '<br />';

				// echo $bal_p;
				// echo '<br />';

				// echo $leave_amount;
				// echo '<br />';

				// echo $db_value;
				// echo '<br />';

				if($db_value > 18){
					$db_value = 18;
				}
				$bal_to_add = $db_value;
				
				
				$insert_leave_sql = "INSERT INTO `oc_leave` SET 
							`emp_id` = '".$evalue['emp_code']."', 
							`emp_name` = '".$evalue['name']."', 
							`emp_doj` = '".$evalue['doj']."',
							`pl_acc` = '".$bal_to_add."',
							`year` = '".date('Y')."',
							`close_status` = '0' "; 
				// echo $insert_leave_sql;
				// echo '<br />';
				// $new_leave_id = 1;
				query($insert_leave_sql, $conn);
				$new_leave_id = getLastId($conn);

				$update_new_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$new_leave_id."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."', `value_open` = '".$bal_to_add."' ";
				// echo $update_new_sql;
				// echo '<br />';
				query($update_new_sql, $conn);
				
			} else {
				$bal_to_add = $bal_to_add;
				
				$update_old_leave_sql = "UPDATE `oc_leave` SET `close_status` = '0' WHERE `leave_id` = '".$is_exist_leave['leave_id']."' ";
				// echo $update_old_leave_sql;
				// echo '<br />';
				query($update_old_leave_sql, $conn);				

				$insert_leave_sql = "INSERT INTO `oc_leave` SET 
							`emp_id` = '".$evalue['emp_code']."', 
							`emp_name` = '".$evalue['name']."', 
							`emp_doj` = '".$evalue['doj']."',
							`pl_acc` = '".$bal_to_add."',
							`year` = '".date('Y')."',
							`close_status` = '0' "; 
				// echo $insert_leave_sql;
				// echo '<br />';
				// $new_leave_id = 1;
				query($insert_leave_sql, $conn);
				$new_leave_id = getLastId($conn);

				$update_new_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$new_leave_id."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."', `value_open` = '".$bal_to_add."' ";
				// echo $update_new_sql;
				// echo '<br />';
				query($update_new_sql, $conn);
			}
			// echo '<br />';
			// echo '<br />';
			// echo 'out';exit;
		}
		//echo 'out';exit;
	}
	echo 'out';exit;
	//echo 'Done';
	//exit;
//}
$conn->close();
echo 'Done';exit;
?>