<?php
$servername = "localhost";
$username = "root";
$password = "JmC@2018";
$dbname = "db_attendance_jmc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

function getLastId($conn){
	return $conn->insert_id;
}

function query($sql, $conn) {
	$query = $conn->query($sql);

	if (!$conn->errno){
		if (isset($query->num_rows)) {
			$data = array();

			while ($row = $query->fetch_assoc()) {
				$data[] = $row;
			}

			$result = new stdClass();
			$result->num_rows = $query->num_rows;
			$result->row = isset($data[0]) ? $data[0] : array();
			$result->rows = $data;

			unset($data);

			$query->close();

			return $result;
		} else{
			return true;
		}
	} else {
		throw new ErrorException('Error: ' . $conn->error . '<br />Error No: ' . $conn->errno . '<br />' . $sql);
		exit();
	}
}
if(date('j') === '7') {
	$today_date = date('Y-m-d');
	//$today_date = '2019-01-07';
	$compare_dates = date('Y-m-26');
	//$compare_dates = '2019-01-26';
	$compare_date = date('Y-m-d', strtotime($compare_dates.' -2 month'));
	$employee_datas_sql = "SELECT  `emp_code`, `company`, `company_id`, `region`, `region_id`, `division`, `division_id`, `department`, `department_id`, `unit`, `unit_id`, `doj`, `name`  FROM `oc_employee` WHERE `company_id` <> '1' AND (DATE(`dol`) = '0000-00-00' OR DATE(`dol`) > '".$compare_date."') ";
	//echo $employee_datas_sql;exit;
	$employee_datas = query($employee_datas_sql, $conn)->rows;
	// echo '<pre>';
	// print_r($employee_datas_sql);
	// exit;
	$from_dates = date('Y-m-26');
	//$from_dates = '2019-01-26';
	$from_date = date('Y-m-d', strtotime($from_dates.' -2 month'));
	$to_dates = date('Y-m-25');
	//$to_dates = '2019-01-25';
	$to_date = date('Y-m-d', strtotime($to_dates.' -1 month'));

	$current_month = date('n', strtotime($to_date . ' +0 month'));
	if($current_month == 1){
		$current_year = date('Y', strtotime($to_date . ' +0 month'));
	} else {
		$current_year = date('Y', strtotime($to_date . ' +0 month'));
	}
	//echo $employee_datas_sql;exit;
	foreach($employee_datas as $ekey => $evalue){
		$transaction_datas = query("SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$evalue['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' ", $conn)->rows;
		// echo '<pre>';
		// print_r($transaction_datas);
		// exit;
		//echo "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$evalue['emp_code']."' AND `date` >= '".$from_date."' AND `date` <= '".$to_date."' ";exit;
		$present_count = 0;
		$leave_count = 0;
		$count_t = count($transaction_datas) - 1;
		foreach($transaction_datas as $tkey => $tvalue){
			if($tvalue['leave_status'] == '1'){
				if($tvalue['firsthalf_status'] == 'OD'){
					$present_count = $present_count + 1;
				}
				if($tvalue['firsthalf_status'] == 'PL'){
					$leave_count = $leave_count + 1;
					$present_count = $present_count + 1;
				}
			} elseif($tvalue['leave_status'] == '0.5'){
				if($tvalue['firsthalf_status'] == 'OD'){
					$present_count = $present_count + 0.5;
					if($tvalue['secondhalf_status'] == '1'){
						$present_count = $present_count + 0.5;
					}
				}
				if($tvalue['secondhalf_status'] == 'OD'){
					$present_count = $present_count + 0.5;
					if($tvalue['firsthalf_status'] == '1'){
						$present_count = $present_count + 0.5;
					}
				}
				if($tvalue['firsthalf_status'] == 'PL'){
					$leave_count = $leave_count + 0.5;
					$present_count = $present_count + 0.5;
					if($tvalue['secondhalf_status'] == '1'){
						$present_count = $present_count + 0.5;
					}
				}
				if($tvalue['secondhalf_status'] == 'PL'){
					$leave_count = $leave_count + 0.5;
					$present_count = $present_count + 0.5;
					if($tvalue['firsthalf_status'] == '1'){
						$present_count = $present_count + 0.5;
					}
				}
			} elseif($tvalue['weekly_off'] != '0'){
				if($tkey == 0){
					$last_key = $tkey;
					$last_key_1 = $last_key;
				} else {
					$last_key = $tkey - 1;
					if($last_key == 0){
						$last_key_1 = $last_key;
					} else {
						$last_key_1 = $last_key - 1;
					}
				}
				if($count_t == $tkey){
					$next_key = $tkey;
					$next_key_1 = $next_key;
				} else {
					$next_key = $tkey + 1;	
					$next_key_1 = $next_key + 1;
				}
				
				if(isset($transaction_datas[$last_key]['company_id']) && $transaction_datas[$last_key]['company_id'] <> '1'){
					if($transaction_datas[$last_key]['act_intime'] != '00:00:00' && $transaction_datas[$last_key]['act_outtime'] == '00:00:00'){
						$last_exception = '1';
					} else {
						$last_exception = '0';
					}
				} else {
					$last_exception = '0';
				}

				if(isset($transaction_datas[$next_key]['company_id']) && $transaction_datas[$next_key]['company_id'] <> '1'){
					if($transaction_datas[$next_key]['act_intime'] != '00:00:00' && $transaction_datas[$next_key]['act_outtime'] == '00:00:00'){
						$next_exception = '1';
					} else {
						$next_exception = '0';
					}
				} else {
					$next_exception = '0';
				}

				if(isset($transaction_datas[$last_key_1]['company_id']) && $transaction_datas[$last_key_1]['company_id'] <> '1'){
					if($transaction_datas[$last_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$last_key_1]['act_outtime'] == '00:00:00'){
						$last_exception_1 = '1';
					} else {
						$last_exception_1 = '0';
					}
				} else {
					$last_exception_1 = '0';
				}

				if(isset($transaction_datas[$next_key_1]['company_id']) && $transaction_datas[$next_key_1]['company_id'] <> '1'){
					if($transaction_datas[$next_key_1]['act_intime'] != '00:00:00' && $transaction_datas[$next_key_1]['act_outtime'] == '00:00:00'){
						$next_exception_1 = '1';
					} else {
						$next_exception_1 = '0';
					}
				} else {
					$next_exception_1 = '0';
				}

				if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception == '0' && $next_exception == '0'){
				} else {
					if($transaction_datas[$next_key]['weekly_off'] != '0'){
						if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key_1]['absent_status'] == '1' && $last_exception == '0' && $next_exception_1 == '0'){
						} else {
							$present_count = $present_count + 1;		
						}
					} else {
						if($transaction_datas[$last_key]['weekly_off'] != '0'){
							if($transaction_datas[$last_key_1]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1' && $last_exception_1 == '0' && $next_exception == '0'){
							} else {
								$present_count = $present_count + 1;		
							}	
						} else {
							$present_count = $present_count + 1;	
						}
					}
				}
			} elseif($tvalue['holiday_id'] != '0'){
				$present_count = $present_count + 1;
			} elseif($tvalue['present_status'] == '1'){
				$present_count = $present_count + 1;
			} elseif($tvalue['present_status'] == '0.5'){
				$present_count = $present_count + 0.5;
			} elseif($tvalue['act_intime'] != '00:00:00' && $tvalue['act_outtime'] == '00:00:00'){
				$present_count = $present_count + 1;
			}
		}
		
		// echo $evalue['emp_code'];
		// echo '<br />';
		// echo $present_count;
		// echo '<br />';
		// exit;

		if($present_count >= '9.5'){
			if($present_count >= '9.5' && $present_count <= '14.5'){
				$bal_to_add = 0.5;
			} elseif($present_count >= '15' && $present_count <= '24.5'){
				$bal_to_add = 1;	
			} else {
				$bal_to_add = 1.5;
			}

			// echo $bal_to_add;
			// echo '<br />';
			// exit;
			if(date('n') == 4){
				//echo 'aaaa';exit;
				$is_exist_leaves = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
				if($is_exist_leaves->num_rows > 0){
					$is_exist_leave = $is_exist_leaves->row;
					
					$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
					if($is_exist_pls->num_rows > 0){
						$update_sql = "UPDATE `oc_leave_employee` SET `value` = (`value` + ".$bal_to_add.") WHERE `id` = '".$is_exist_pls->row['id']."' ";
					} else {
						$update_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$is_exist_leave['leave_id']."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."' , `value_open` = '".$bal_to_add."' ";
					}
					// echo $update_sql;
					// echo '<br />';
					//exit;
					query($update_sql, $conn);

					$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
					if($is_exist_pls->num_rows > 0){
						$is_exist_pl = $is_exist_pls->row;
						$update_old_leave_sql = "UPDATE `oc_leave` SET `close_status` = '1' WHERE `leave_id` = '".$is_exist_leave['leave_id']."' ";
						// echo $update_old_leave_sql;
						// echo '<br />';
						query($update_old_leave_sql, $conn);

						$db_value = $is_exist_pl['value'];
						$acc_value = $is_exist_pl['value'];
						$year = $is_exist_leave['year'];
						$year_1 = $is_exist_leave['year'] + 1;
						$comp_date = $year.'-03-26';
						$comp_date_end = $year_1.'-03-25';
						
						$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction` WHERE `emp_id` = '" . $evalue['emp_code'] . "' AND `leave_amount` = '' AND `leave_type`= 'PL' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' AND date(`date`) <= '".$comp_date_end."' ";
						// echo $sql;
						// echo '<br />';
						$leave_1_datas = query($sql, $conn);
						$bal_p = 0;
						if($leave_1_datas->num_rows > 0){
							$leave_1_data = $leave_1_datas->row;
							$bal_p = $leave_1_data['bal_p'];
							if($bal_p > 0){
							} else {
								$bal_p = 0;
							}
						}

						$comp_date = $year.'-03-26';
						$comp_date_end = $year_1.'-03-25';
						$sql1 = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $evalue['emp_code'] . "' AND `days` = '' AND `leave_type`= 'PL' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' AND date(`date`) <= '".$comp_date_end."' ";
						// echo $sql1;
						// echo '<br />';
						$leave_2_datas = query($sql1, $conn);
						$leave_amount = 0;
						if($leave_2_datas->num_rows > 0){
							$leave_2_data = $leave_2_datas->row;
							$leave_amount = $leave_2_data['leave_amount'];
							if($leave_amount > 0){
							} else {
								$leave_amount = 0;
							}
						}
						$total_bal = $acc_value - ($bal_p + $leave_amount);
						$db_value = $total_bal + $bal_to_add;
						
						if($db_value > 18){
							$db_value = 18;
						}
						$bal_to_add = $db_value;
						
						$insert_leave_sql = "INSERT INTO `oc_leave` SET 
									`emp_id` = '".$evalue['emp_code']."', 
									`emp_name` = '".$evalue['name']."', 
									`emp_doj` = '".$evalue['doj']."',
									`pl_acc` = '".$bal_to_add."',
									`year` = '".date('Y')."',
									`close_status` = '0' "; 
						// echo $insert_leave_sql;
						// echo '<br />';
						// $new_leave_id = 1;
						query($insert_leave_sql, $conn);
						$new_leave_id = getLastId($conn);

						$update_new_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$new_leave_id."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."', `value_open` = '".$bal_to_add."' ";
						// echo $update_new_sql;
						// echo '<br />';
						query($update_new_sql, $conn);

					}  else {
						$bal_to_add = $bal_to_add;
					
						$update_old_leave_sql = "UPDATE `oc_leave` SET `close_status` = '0' WHERE `leave_id` = '".$is_exist_leave['leave_id']."' ";
						// echo $update_old_leave_sql;
						// echo '<br />';
						query($insert_leave_sql, $conn);				

						$insert_leave_sql = "INSERT INTO `oc_leave` SET 
									`emp_id` = '".$evalue['emp_code']."', 
									`emp_name` = '".$evalue['name']."', 
									`emp_doj` = '".$evalue['doj']."',
									`pl_acc` = '".$bal_to_add."',
									`year` = '".date('Y')."',
									`close_status` = '0' "; 
						// echo $insert_leave_sql;
						// echo '<br />';
						//$new_leave_id = 1;
						query($insert_leave_sql, $conn);
						$new_leave_id = getLastId($conn);

						$update_new_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$new_leave_id."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."', `value_open` = '".$bal_to_add."' ";
						// echo $update_new_sql;
						// echo '<br />';
						query($update_new_sql, $conn);
					}
				}
				$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `name` = '".$evalue['name']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `company` = '".$evalue['company']."', `company_id` = '".$evalue['company_id']."', `region` = '".$evalue['region']."', `region_id` = '".$evalue['region_id']."', `division` = '".$evalue['division']."', `division_id` = '".$evalue['division_id']."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."', `present_count` = '".$present_count."', `leave_taken` = '".$leave_count."' ";
				query($insert_sql, $conn);
			} else {
				$is_exist_leaves = query("SELECT * FROM `oc_leave` WHERE `emp_id` = '".$evalue['emp_code']."' AND `close_status` = '0' ", $conn);
				if($is_exist_leaves->num_rows > 0){
					//$leave_credit_counts = query("SELECT * FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$evalue['emp_code']."' ", $conn);
					//if($leave_credit_counts->num_rows > 1){
						$is_exist_leave = $is_exist_leaves->row;
						$is_exist_pls = query("SELECT * FROM `oc_leave_employee` WHERE `leave_id` = '".$is_exist_leave['leave_id']."' AND `leavename` = 'PL' ", $conn);
						if($is_exist_pls->num_rows > 0){
							$update_sql = "UPDATE `oc_leave_employee` SET `value` = (`value` + ".$bal_to_add.") WHERE `id` = '".$is_exist_pls->row['id']."' ";
						} else {
							$update_sql = "INSERT INTO `oc_leave_employee` SET `leave_id` = '".$is_exist_leave['leave_id']."', `emp_id` = '".$is_exist_leave['emp_id']."', `leavename` = 'PL', `value` = '".$bal_to_add."' , `value_open` = '".$bal_to_add."' ";
						}
						// echo $update_sql;
						// echo '<br />';
						// echo '<br />';
						// exit;
						query($update_sql, $conn);
					//}
					
					$insert_sql = "INSERT INTO `oc_leave_credit_transaction` SET `emp_code` = '".$evalue['emp_code']."', `name` = '".$evalue['name']."', `leave_value` = '".$bal_to_add."', `leave_name` = 'PL', `month` = '".$current_month."', `year` = '".$current_year."', `dot` = '".$today_date."', `company` = '".$evalue['company']."', `company_id` = '".$evalue['company_id']."', `region` = '".$evalue['region']."', `region_id` = '".$evalue['region_id']."', `division` = '".$evalue['division']."', `division_id` = '".$evalue['division_id']."', `department` = '".$evalue['department']."', `department_id` = '".$evalue['department_id']."', `unit` = '".$evalue['unit']."', `unit_id` = '".$evalue['unit_id']."', `present_count` = '".$present_count."', `leave_taken` = '".$leave_count."' ";
					query($insert_sql, $conn);
					// echo $insert_sql;
					// echo '<br />';
					// echo '<br />';
					//exit;
				}
			}
			//echo 'out';exit;
		}
	}
	//echo 'Done';
	//exit;
}
$conn->close();
echo 'Done';exit;
?>