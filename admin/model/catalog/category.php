<?php
class ModelCatalogCategory extends Model {
	public function addcategory($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "category` SET 
							`category_name` = '" . $this->db->escape(html_entity_decode($data['category_name'])) . "',
							`category_code` = '" . $this->db->escape(html_entity_decode($data['category_code'])) . "'
						");

		$category_id = $this->db->getLastId(); 
	}

	public function editcategory($category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "category SET 
							`category_name` = '" . $this->db->escape(html_entity_decode($data['category_name'])) . "',
							`category_code` = '" . $this->db->escape(html_entity_decode($data['category_code'])) . "'
							WHERE category_id = '" . (int)$category_id . "'");
	}

	public function deletecategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
	}	

	public function getcategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");

		return $query->row;
	}

	public function getcategorys($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "category WHERE 1=1 ";
		//echo '<pre>';print_r($data);exit;
		/*if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND category_id = '" . $data['filter_name_id'] . "' ";
		}*/

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(category_name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'category_name',
			'category_code',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY category_name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalcategorys() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND category_id = '" . $data['filter_name_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(category) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>