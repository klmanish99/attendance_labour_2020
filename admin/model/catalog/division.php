<?php
class ModelCatalogDivision extends Model {
	public function addDivision($data) {
		$region_name = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$data['region_id']."' ")->row['region'];
		$this->db->query("INSERT INTO `" . DB_PREFIX . "division` SET 
							`division` = '" . $this->db->escape(html_entity_decode($data['division'])) . "',
							`division_code` = '" . $this->db->escape(html_entity_decode($data['division_code'])) . "',
							`region_id` = '" . $this->db->escape($data['region_id']) . "',
							`region_name` = '" . $this->db->escape(html_entity_decode($region_name)) . "'
						");

		$division_id = $this->db->getLastId(); 
	}

	public function editDivision($division_id, $data) {
		$region_name = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$data['region_id']."' ")->row['region'];
		$this->db->query("UPDATE " . DB_PREFIX . "division SET 
							`division` = '" . $this->db->escape(html_entity_decode($data['division'])) . "',
							`division_code` = '" . $this->db->escape(html_entity_decode($data['division_code'])) . "',
							`region_id` = '" . $this->db->escape($data['region_id']) . "',
							`region_name` = '" . $this->db->escape(html_entity_decode($region_name)) . "'
							WHERE division_id = '" . (int)$division_id . "'");

		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							`division` = '" . $this->db->escape(html_entity_decode($data['division'])) . "',
							`division_id` = '" . $this->db->escape(html_entity_decode($division_id)) . "'
							WHERE division_id = '" . (int)$division_id . "'");
	}

	public function deleteDivision($division_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "division WHERE division_id = '" . (int)$division_id . "'");
	}	

	public function getDivision($division_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "division WHERE division_id = '" . (int)$division_id . "'");

		return $query->row;
	}

	public function getDivisions($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "division WHERE 1=1 ";

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND division_id = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_region_id']) && !empty($data['filter_region_id'])) {
			$sql .= " AND region_id = '" . $data['filter_region_id'] . "' ";
		}

		if (isset($data['filter_region_ids']) && !empty($data['filter_region_ids'])) {
			$region_string = "'" . str_replace(",", "','", html_entity_decode($data['filter_region_ids'])) . "'";
			$sql .= " AND region_id IN (" . strtolower($region_string) . ") ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(division) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		
		$sort_data = array(
			'division',
			'division_code',
			'region_name',
		);		

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY division";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalDivisions() {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "division";
		
		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND division_id = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_region_id']) && !empty($data['filter_region_id'])) {
			$sql .= " AND region_id = '" . $data['filter_region_id'] . "' ";
		}

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(division) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
}
?>