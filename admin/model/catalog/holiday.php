<?php
class ModelCatalogholiday extends Model {
	public function addholiday($data) {
		$unit_datas = $this->db->query("SELECT `unit`, `unit_id` FROM `oc_unit` GROUP BY `unit`")->rows;
		$unit_data = array();
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
				}
			} else {
				$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
			}
		}
		// echo '<pre>';
		// print_r($data);
		// exit;

		$this->db->query("INSERT INTO " . DB_PREFIX . "holiday SET `name` = '" . $this->db->escape($data['name']) . "', `date` = '" . $this->db->escape($data['date']) . "', `division_datas` = '".$this->db->escape((isset($data['holi_division_datas'])) ? serialize($data['holi_division_datas']) : '')."' ");
		$holiday_id = $this->db->getLastId();	
		$day_date = date('j', strtotime($data['date']));
		$month_date = date('n', strtotime($data['date'])); 
		$holiday_ids = 'H_'.$holiday_id;
		if(isset($data['holi_datas'])){
			$sql = ''; 
			foreach ($data['holi_datas'] as $keys => $values) {
				//$sql = $this->db->query("INSERT INTO oc_holiday_loc SET `holiday_id` = '".$holiday_id."', `location` = '".$unit_data[$keys]."', `value` = '".serialize($values)."' ");
				$sql .= "INSERT INTO oc_holiday_loc SET `holiday_id` = '".$holiday_id."', `location` = '".$unit_data[$keys]."', `location_id` = '".$keys."', `value` = '".serialize($values)."';";
				foreach ($values as $key => $value) {
					$value = html_entity_decode(strtolower(trim($value)));
					$emp_codes = $this->db->query("SELECT `emp_code`, `doj`, `shift_id` FROM `oc_employee` WHERE `unit_id` = '".$keys."' AND `department_id` = '".$key."' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						if(strtotime($data['date']) > strtotime($evalue['doj'])){
							$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."'AND `month`= '".$month_date."' AND `unit_id`= '".$keys."'  ");
							$current_shift = 'S_1';
							if($current_shifts->num_rows > 0){
								$current_shift = $current_shifts->row[$day_date];
							}
							$current_shift_exp = explode('_', $current_shift);
							$holiday_idss = $holiday_ids;
							if($current_shift_exp[0] == 'S' || $current_shift_exp[0] == 'HD'){
								if(isset($current_shift_exp[1]) && $current_shift_exp[1] != ''){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
								} else {
									$holiday_idss = $holiday_ids.'_'.$evalue['shift_id'];
								}
							}
							//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$keys."' ");
							$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$keys."';";
						}
					}
				}
			}
			$this->new_mysql($sql);
		}
	}

	public function new_mysql($sql) {
		$con=mysqli_connect("localhost","root","JmC@2018","db_attendance_jmc");
		mysqli_multi_query($con,$sql);
		// do {
		//     if($result = mysqli_store_result($con)){
		//         mysqli_free_result($result);
		//     }
		// } while(mysqli_next_result($con));
		if(mysqli_error($con)) {
		    die(mysqli_error($con));
		}
		// while (mysqli_next_result($link)) {
		// 	if (!mysqli_more_results()){
		// 		break;
		// 	}
		// }
		mysqli_close($con);
	}

	public function editholiday($holiday_id, $data) {
		$unit_datas = $this->db->query("SELECT `unit`, `unit_id` FROM `oc_unit` GROUP BY `unit`")->rows;
		$unit_data = array();
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
				}
			} else {
				$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
			}
		}

		$week_data = $this->getholiday($holiday_id);
		$shift_units = $this->db->query("SELECT `location`, `value`, `location_id` FROM " . DB_PREFIX . "holiday_loc WHERE holiday_id = '" . (int)$holiday_id . "' ");
		foreach ($shift_units->rows as $ukey => $uvalue) {
			$database_location[$uvalue['location_id']] = unserialize($uvalue['value']);
		}
		if (!isset($database_location)) {
			$database_location = array();
		}

		// echo '<pre>';
		// print_r($data['holi_datas']);
		// echo '<pre>';
		// print_r($database_location);
		// exit;

		$this->db->query("UPDATE " . DB_PREFIX . "holiday SET `name` = '" . $this->db->escape($data['name']) . "', `date` = '" . $this->db->escape($data['date']) . "', `division_datas` = '".$this->db->escape((isset($data['holi_division_datas'])) ? serialize($data['holi_division_datas']) : '')."' WHERE holiday_id = '" . (int)$holiday_id . "' ");
		$holiday_ids = 'H_'.$holiday_id;
		$day_date = date('j', strtotime($data['date']));
		$month_date = date('n', strtotime($data['date'])); 

		$sql = '';
		$units = $this->db->query("SELECT `unit`, `unit_id` FROM " . DB_PREFIX . "unit");
		if ($units->num_rows > 0) { 	
			if (isset($data['holi_datas'])) {
			  	foreach ($units->rows as $key => $value) { 
					$d_dept = array();
					if(isset($database_location[$value['unit_id']])){
						if(isset($data['holi_datas'][$value['unit_id']])){
							//$sql = '';
							foreach ($data['holi_datas'][$value['unit_id']] as $dkey => $dvalue) { //post
								$dvalue = html_entity_decode(strtolower(trim($dvalue)));
								if(!in_array($dvalue, $d_dept)){  
									$d_dept[] = $dvalue;
									$emp_codes =$this->db->query("SELECT `emp_code`, `doj`, `shift_id` FROM `oc_employee` WHERE `department_id` = '".$dkey."' AND unit_id = '".$value['unit_id']."' ");
									foreach ($emp_codes->rows as $ekey => $evalue) { 
										if(strtotime($data['date']) > strtotime($evalue['doj'])){
											$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."'  ");
											$current_shift = 'S_1';
											if($current_shifts->num_rows > 0){
												$current_shift = $current_shifts->row[$day_date];
											}
											$current_shift_exp = explode('_', $current_shift);
											$holiday_idss = $holiday_ids;
											if($current_shift_exp[0] == 'S' || $current_shift_exp[0] == 'HD'){
												if(isset($current_shift_exp[1]) && $current_shift_exp[1] != ''){
													$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
												} else {
													$holiday_idss = $holiday_ids.'_'.$evalue['shift_id'];
												}
												//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."' ");
												$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."'; ";
											}
										}
									}
								}  
							}
							if($sql != ''){
								//$this->new_mysql($sql);
							}
						}
						if(isset($database_location[$value['unit_id']])){
							//$sql = '';
							foreach ($database_location[$value['unit_id']] as $d1key => $d1value) { 
								$d1value = html_entity_decode(strtolower(trim($d1value))); 
								if(!in_array($d1value, $d_dept)){ 
									$emp_codes = $this->db->query("SELECT `emp_code`, `shift_id` FROM `oc_employee` WHERE `department_id` = '".$d1key."' AND `unit_id` = '".$value['unit_id']."' ");
									foreach ($emp_codes->rows as $ekey => $evalue) {
										$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."'AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."'  ");
										$current_shift = 'S_1';
										if($current_shifts->num_rows > 0){
											$current_shift = $current_shifts->row[$day_date];
										}
										$current_shift_exp = explode('_', $current_shift);
										if($current_shift_exp[0] != 'S' || $current_shift_exp[0] == 'HD'){
											if(isset($current_shift_exp[2]) && $current_shift_exp[2] != ''){
												$holiday_idss = 'S_'.$current_shift_exp[2];
											} else {
												$holiday_idss = 'S_'.$evalue['shift_id'];
											}
											//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."' ");
											$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."'; ";
										}
									}
								}
							}
							if($sql != ''){
								//$this->new_mysql($sql);
							}
						}
					} else {
						if(isset($data['holi_datas'][$value['unit_id']])){
							//$sql = ''; 
							foreach ($data['holi_datas'][$value['unit_id']] as $dkey => $dvalue) { 
								$dvalue1 = html_entity_decode(strtolower(trim($dvalue)));
								$emp_codes = $this->db->query("SELECT `emp_code`, `doj`, `shift_id` FROM `oc_employee` WHERE `department_id` = '".$dkey."' AND `unit_id` = '".$value['unit_id']."' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									if(strtotime($data['date']) > strtotime($evalue['doj'])){
										$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."'AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."'  ");
										$current_shift = 'S_1';
										if($current_shifts->num_rows > 0){
											$current_shift = $current_shifts->row[$day_date];
										}
										$current_shift_exp = explode('_', $current_shift);
										$holiday_idss = $holiday_ids;
										if($current_shift_exp[0] == 'S' || $current_shift_exp[0] == 'HD'){
											if(isset($current_shift_exp[1]) && $current_shift_exp[1] != ''){
												$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
											} else {
												$holiday_idss = $holiday_ids.'_'.$evalue['shift_id'];
											}
										}
										//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."' ");
										$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$value['unit_id']."'; ";
									}
								} 
							}
							if($sql != ''){
								//$this->new_mysql($sql);
							}
						}
					}
			  	}
			} else { 
				//$sql = '';
				$d_dept = array();
				foreach ($database_location as $d1key => $d1value) { 
					foreach ($d1value as $dkey => $dvalue) { 		 
						$dvalue = html_entity_decode(strtolower(trim($dvalue))); //exit;
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code`, `shift_id` FROM `oc_employee` WHERE `department_id` = '".$dkey."' AND `unit_id` = '".$d1key."' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."'AND `month`= '".$month_date."' AND `unit_id`= '".$d1key."'  ");
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								} 
								$current_shift_exp = explode('_', $current_shift);
								if($current_shift_exp[0] != 'S' || $current_shift_exp[0] == 'HD'){
									if(isset($current_shift_exp[2]) && $current_shift_exp[2] != ''){
										$holiday_idss = 'S_'.$current_shift_exp[2];
									} else {
										$holiday_idss = 'S_'.$evalue['shift_id'];
									}
									//$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$d1key."' ");
									$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month`= '".$month_date."' AND `unit_id`= '".$d1key."'; ";
								}
							}
						}
					}
				}
				if($sql != ''){
					//$this->new_mysql($sql);
				}
			}
		}

		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "holiday_loc WHERE holiday_id = '" . (int)$holiday_id . "'");
		if(isset($data['holi_datas'])){
			//$sql = '';
			foreach ($data['holi_datas'] as $keys => $values) {
				$sql .= "INSERT INTO oc_holiday_loc SET `holiday_id` = '".$holiday_id."', `location` = '".$unit_data[$keys]."', `location_id` = '".$keys."', `value` = '".serialize($values)."'; ";
			}
			if($sql != ''){
				//$this->new_mysql($sql);
			}
		}
		
		$this->log->write($sql);

		if($sql != ''){
			$this->new_mysql($sql);
		}
	}

	public function deleteholiday($holiday_id) {
		//$this->db->query("DELETE FROM " . DB_PREFIX . "employee_meta WHERE holiday_id = '" . (int)$holiday_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
	}	

	public function getholiday($holiday_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
		return $query->row;
	}

	public function getholidays($data = array()) { 
		$sql = "SELECT * FROM " . DB_PREFIX . "holiday WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		$sort_data = array(
			'name',
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}				

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getholiday_exist($date) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "holiday WHERE `date` = '" . $date . "'");
		if($query->row['total'] > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function getTotalholidays($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "holiday WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>