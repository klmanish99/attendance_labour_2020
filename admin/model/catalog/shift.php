<?php
class ModelCatalogShift extends Model {
	public function addshift($data) {
		$unit_datas = $this->db->query("SELECT `unit`, `unit_id` FROM `oc_unit` GROUP BY `unit`")->rows;
		$unit_data = array();
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
				}
			} else {
				$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
			}
		}
		$this->db->query("INSERT INTO " . DB_PREFIX . "shift SET `name` = '" . $this->db->escape($data['name']) . "', `shift_code` = '" . $this->db->escape($data['shift_code']) . "', `in_time` = '" . $this->db->escape($data['in_time']) . "', `out_time` = '" . $this->db->escape($data['out_time']) . "', `weekly_off_1` = '" . $this->db->escape($data['weekly_off_1']) . "', `weekly_off_2` = '" . $this->db->escape($data['weekly_off_2']) . "', `lunch` = '".$data['lunch']."' ");
		$shift_id = $this->db->getLastId();

		if(isset($data['shift_datas'])){
			$sql = '';
			foreach ($data['shift_datas'] as $keys => $values) {
				$unit= $this->db->query("INSERT INTO oc_shift_unit SET `shift_id` = '".$shift_id."', `name_id` = '".$keys."', `name` = '".$unit_data[$keys]."', `value` = '".serialize($values)."' ");
				foreach ($values as $key => $value) {
					$value = html_entity_decode(strtolower(trim($value)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `unit_id` = '".$keys."' AND `department_id` = '".$key."' ");
					$shift = 'S_'.$shift_id;
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
						foreach($shift_s_datas->rows as $skeys => $svalues){
							foreach ($svalue as $skey => $svalue) {
								if($skey != 'id' && $akey != 'emp_code' && $akey != 'month' && $akey != 'year' && $akey != 'status' && $akey != 'unit' && $akey != 'unit_id'){
									$s_data_exp = explode("_", $svalue);
									if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
										foreach($s_data_exp as $sdxkey => $sdxvalue){
											if($sdxkey == 2){
												$s_data_exp[$sdxkey] = $shift_id;
											}
										}
										$svalue = implode('_', $s_data_exp);
										$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` = '".$svalues['year']."' AND `unit_id` = '".$svalues['unit_id']."'; ";
									} else {
										$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` = '".$svalues['year']."' AND `unit_id` = '".$svalues['unit_id']."'; ";
									}
								} 
							}
						}
					}
				}
			}
			if($sql != ''){
				$this->new_mysql($sql);
			}
		}
	}

	public function new_mysql($sql) {
		$con=mysqli_connect("localhost","root","JmC@2018","db_attendance_jmc");
		mysqli_multi_query($con,$sql);
		// do {
		//     if($result = mysqli_store_result($con)){
		//         mysqli_free_result($result);
		//     }
		// } while(mysqli_more_results($con) && mysqli_next_result($con));
		if(mysqli_error($con)) {
		    die(mysqli_error($con));
		}
		mysqli_close($con);
	}

	public function editshift($shift_id, $data) {
		$week_data = $this->getshift($shift_id);
		$unit_datas = $this->db->query("SELECT `unit`, `unit_id` FROM `oc_unit` GROUP BY `unit`")->rows;
		$unit_data = array();
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
				}
			} else {
				$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
			}
		}
		$shift_units = $this->db->query("SELECT `name`, `value`, `name_id` FROM " . DB_PREFIX . "shift_unit WHERE shift_id = '" . (int)$shift_id . "' ");
		foreach ($shift_units->rows as $ukey => $uvalue) {
			$database_location[$uvalue['name_id']] = unserialize($uvalue['value']);
		}
		if (!isset($database_location)) {
			$database_location = array();
		}
		$this->db->query("UPDATE " . DB_PREFIX . "shift SET `name` = '" . $this->db->escape($data['name']) . "', `shift_code` = '" . $this->db->escape($data['shift_code']) . "', `in_time` = '" . $this->db->escape($data['in_time']) . "', `out_time` = '" . $this->db->escape($data['out_time']) . "', `weekly_off_1` = '" . $this->db->escape($data['weekly_off_1']) . "', `weekly_off_2` = '" . $this->db->escape($data['weekly_off_2']) . "', `lunch` = '".$data['lunch']."' WHERE shift_id = '" . (int)$shift_id . "'");
		$r_shift_id = $shift_id;
		$shift_id = 'S_'.$shift_id;

		$units = $this->db->query("SELECT `unit`, `unit_id` FROM " . DB_PREFIX . "unit");
		$sql = '';
		if ($units->num_rows > 0) { 	
			if (isset($data['shift_datas'])) {
				foreach ($units->rows as $key => $value) {
					$d_dept = array();
					if(isset($database_location[$value['unit_id']])){
						if(isset($data['shift_datas'][$value['unit_id']])){
							//$sql = '';
							foreach ($data['shift_datas'][$value['unit_id']] as $dkey => $dvalue) {
								$dvalue = html_entity_decode(strtolower(trim($dvalue)));
								if(!in_array($dvalue, $d_dept)){  
									$d_dept[] = $dvalue;
									$emp_codes =$this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `department_id` = '".$dkey."' AND `unit_id` = '".$value['unit_id']."' ");
									foreach ($emp_codes->rows as $ekey => $evalue) { 
										$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
										foreach($shift_s_datas->rows as $skeys => $svalues){
											foreach($svalues as $skey => $svalue){
												if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
													$s_data_exp = explode('_', $svalue);
													if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
														foreach($s_data_exp as $sdxkey => $sdxvalue){
															if($sdxkey == 2){
																$s_data_exp[$sdxkey] = $r_shift_id;
															}
														}
														$svalue = implode('_', $s_data_exp);
														$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$svalues['unit_id']."'; ";
													} else {
														$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$svalues['unit_id']."'; ";
													}
												}
											} 
										}
									}
								}  
							}
							if ($sql != '') {
								//$this->new_mysql($sql);
							}
						}
						
						if(isset($database_location[$value['unit_id']])){
							//$sql = '';
							foreach ($database_location[$value['unit_id']] as $d1key => $d1value) { 
								$d1value = html_entity_decode(strtolower(trim($d1value)));
								if(!in_array($d1value, $d_dept)){
									$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `department_id` = '".$d1key."' AND `unit_id` = '".$value['unit_id']."' ");
									foreach ($emp_codes->rows as $ekey => $evalue) {
										$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
										foreach($shift_s_datas->rows as $skeys => $svalues){
											foreach($svalues as $skey => $svalue){
												if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
													$s_data_exp = explode('_', $svalue);
													if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
														foreach($s_data_exp as $sdxkey => $sdxvalue){
															if($sdxkey == 2){
																$s_data_exp[$sdxkey] = 1;
															}
														}
														$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$svalues['unit_id']."' ;";
													} else {
														$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$svalues['unit_id']."' ;";
													}
												}
											}
										}
									}
								}
							}   
							if ($sql != '') {
								//$this->new_mysql($sql_1);
							}
						}
					} else { 
						if(isset($data['shift_datas'][$value['unit_id']])){
							//$sql = '';
							foreach ($data['shift_datas'][$value['unit_id']] as $dkey => $dvalue) {
								$dvalue = html_entity_decode(strtolower(trim($dvalue)));
								$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `department_id` = '".$dkey."' AND `unit_id` = '".$value['unit_id']."' ");
								foreach ($emp_codes->rows as $ekey => $evalue) {
									$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
									foreach($shift_s_datas->rows as $skeys => $svalues){
										foreach($svalues as $skey => $svalue){
											if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
												$s_data_exp = explode('_', $svalue);
												if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
													foreach($s_data_exp as $sdxkey => $sdxvalue){
														if($sdxkey == 2){
															$s_data_exp[$sdxkey] = $r_shift_id;
														}
													}
													$svalue = implode('_', $s_data_exp);
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$svalues['unit_id']."'; ";
												} else {
													$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($shift_id) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$svalues['unit_id']."'; ";
												}
											}
										}
									}
								} 
							}
							if ($sql != '') {
								//$this->new_mysql($sql_2);
							}
						}
					}
				}
			} else { 
				//$sql = '';
				$d_dept = array();
				foreach ($database_location as $d1key => $d1value) {
					foreach ($d1value as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($d1value, $d_dept)){
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `department_id` = '".$dkey."' AND `unit_id` = '".$d1key."' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								$shift_s_datas = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$evalue['emp_code'] . "' ");
								foreach($shift_s_datas->rows as $skeys => $svalues){
									foreach($svalues as $skey => $svalue){
										if($skey != 'id' && $skey != 'emp_code' && $skey != 'month' && $skey != 'year' && $skey != 'status' && $skey != 'unit' && $skey != 'unit_id'){
											$s_data_exp = explode('_', $svalue);
											if($s_data_exp[0] == 'H' || $s_data_exp[0] == 'HD' || $s_data_exp[0] == 'W' || $s_data_exp[0] == 'C'){
												foreach($s_data_exp as $sdxkey => $sdxvalue){
													if($sdxkey == 2){
														$s_data_exp[$sdxkey] = 1;
													}
												}
												$svalue = implode('_', $s_data_exp);
												$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = '" . $this->db->escape($svalue) . "' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "'AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$d1key."'; ";
											} else {
												$sql .= "UPDATE " . DB_PREFIX . "shift_schedule SET `".$skey."` = 'S_1' WHERE `emp_code` = '" . (int)$evalue['emp_code'] . "' AND `month` = '".$svalues['month']."' AND `year` ='".$svalues['year']."' AND `unit_id`= '".$d1key."'; ";
											}
										}
									}
								}
							}
						}
					}
				}
				if ($sql != '') {
					//$this->new_mysql($sql);
				}
			}
		}

		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "shift_unit WHERE shift_id = '" . (int)$r_shift_id . "'");
		if(isset($data['shift_datas'])){
			foreach ($data['shift_datas'] as $keys => $values) {
				$sql .= "INSERT INTO oc_shift_unit SET `shift_id` = '".$r_shift_id."', `name_id` = '".$keys."', `name` = '".$unit_data[$keys]."', `value` = '".serialize($values)."'; ";
			}
			if ($sql != '') {
				//$this->new_mysql($sql);
			}
		}

		if ($sql != '') {
			$this->new_mysql($sql);
		}
	}

	public function deleteshift($shift_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
	}	

	public function getshift($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		return $query->row;
	}

	public function getshifts($data = array()) { 
		//echo "<pre>"; print_r($data); exit;
		$sql = "SELECT * FROM " . DB_PREFIX . "shift WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		$sort_data = array(
			'name',
			'shift_id'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}				

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalshifts($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "shift WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>
