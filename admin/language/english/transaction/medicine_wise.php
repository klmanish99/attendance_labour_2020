<?php
// Heading
$_['heading_title']      = 'Medicine Wise treatment entry';

// Text
$_['text_success']       = 'Success: You have inserted a Entry!';

// Entry
$_['entry_select_medicine_wise'] = 'Select Medicine:';
$_['entry_dot']       = 'Date:';
$_['entry_add_horse'] = 'Add Horse';
$_['entry_remove']       = 'Remove';
$_['entry_treatment']       = 'Treatment';
$_['entry_price']       = 'Price';
$_['entry_total']       = 'Total';
$_['entry_quantity']       = 'Quantity';
$_['entry_name']       = 'Horse Name';
$_['entry_date']       = 'Date';
$_['entry_trainer']       = 'Trainer';
$_['entry_doctor']       = 'Doctor';
$_['entry_transaction_type'] = 'Transaction Type';

$_['entry_action']       = 'Action';


// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Horse Wise Treatment Entry!';
$_['error_name']         = 'Horse Name must be between 1 and 255 characters!';
$_['error_name_valid']  = 'Please Enter Valid Medicine Name!';
$_['error_medicine_name']  = 'Please Enter Medicine Name!';
$_['error_dot']      	 = 'Please Enter valid date!';
$_['error_select_medicine']  = 'Please Select Medicine!';
$_['error_price']      	 = 'Please Enter Price!';
$_['error_total']      	 = 'Total Cannot be Empty!';
$_['error_horse_empty']      	 = 'Please Select Horses!';
$_['error_medicine_quantity']  = 'Please Enter Quantity Greater then 0!';
$_['error_horse_name']  = 'Please Enter Horse Name!';
$_['error_medicine_exist']  = 'Please Enter Valid Medicine Name!';
?>
