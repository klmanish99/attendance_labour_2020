<?php    
class ControllerCatalogContractor extends Controller { 
			private $error = array();

	public function index() {
		$this->language->load('catalog/contractor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/contractor');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/contractor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/contractor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_contractor->addContractor($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/contractor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/contractor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_contractor->editContractor($this->request->get['contractor_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	public function delete() {

		$this->language->load('catalog/contractor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/contractor');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			$in=0;
			foreach ($this->request->post['selected'] as $contractor_id) {
				$contractor_exits=$this->db->query("SELECT * from  `oc_employee` where `contractor_id`='".$contractor_id."' ");
				if($contractor_exits->num_rows>0){
					$in=1;
					$this->session->data['warning'] = $this->language->get('delete_warning');
					//echo $this->session->data['error'];exit;
				}else{
					$this->model_catalog_contractor->deleteContractor($contractor_id);
 					if($in==0){
						$this->session->data['success'] = $this->language->get('text_success');
					}else{
						unset($this->session->data['success']);
					}	
				}
			}
			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		 /*} elseif(isset($this->request->get['contractor_id']) && $this->validateDelete()){
			$this->model_catalog_contractor->deleteContractor($this->request->get['contractor_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}*/

			//$this->redirect($this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/contractor/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/contractor/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		
		$this->data['export'] = $this->url->link('catalog/contractor/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['contractor'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$contractor_total = $this->model_catalog_contractor->getTotalContractor($data);

		$results = $this->model_catalog_contractor->getContractor_tots($data);

		/*echo '<pre>';
		print_r($results);
		exit;*/

		foreach ($results as $result) {
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/contractor/update', 'token=' . $this->session->data['token'] . '&contractor_id=' . $result['contractor_id'] . $url, 'SSL')
			);
			/*echo '<pre>';
			print_r($action);
			exit; */
			$this->data['contractor'][] = array(
				'contractor_id' => $result['contractor_id'],
				'name'            => $result['contractor_name'].' - '.$result['contractor_code'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['contractor_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		// echo '<pre>';
		// print_r($this->data['contractor']);
		// exit;

		//echo '<pre>';print_r($result);exit;
		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->session->data['warning'])) {
			$this->data['error_warning'] = $this->session->data['warning'];

			unset($this->session->data['warning']);
		} else if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $contractor_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->template = 'catalog/contractor_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['contractor_name'])) {
			$this->data['error_contractor_name'] = $this->error['contractor_name'];
		} else {
			$this->data['error_contractor_name'] = '';
		}

		if (isset($this->error['contractor_code'])) {
			$this->data['error_contractor_code'] = $this->error['contractor_code'];
		} else {
			$this->data['error_contractor_code'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['contractor_id'])) {
			$this->data['action'] = $this->url->link('catalog/contractor/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/contractor/update', 'token=' . $this->session->data['token'] . '&contractor_id=' . $this->request->get['contractor_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL');
		if (isset($this->request->get['contractor_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$contractor_info = $this->model_catalog_contractor->getContractor($this->request->get['contractor_id']);
		}
		
		$data['location'] = array();
		if (isset($this->request->post['location'])) {
			$this->data['location'] = $this->request->post['location'];
		} elseif (!empty($contractor_info)) {
			$contractor_locationss = $this->db->query("SELECT * FROM `oc_contractor_location` WHERE `contractor_id` = '".$contractor_info['contractor_id']."' ");
			$contractor_location = array();
			if($contractor_locationss->num_rows > 0){
				$contractor_locations = $contractor_locationss->rows;
				foreach($contractor_locations as $ckey => $cvalue){
					$contractor_location[] = $cvalue['unit_id'];
				}	
			}
			$this->data['location'] = $contractor_location;
		} else {
			$this->data['location'] = array();
		}
		//echo '<pre>';print_r($data);exit;
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['contractor_name'])) {
			$this->data['contractor_name'] = $this->request->post['contractor_name'];
		} elseif (!empty($contractor_info)) {
			$this->data['contractor_name'] = $contractor_info['contractor_name'];
		} else {	
			$this->data['contractor_name'] = '';
		}

		if (isset($this->request->post['contractor_name'])) {
			$this->data['contractor_name'] = $this->request->post['contractor_name'];
		} elseif (!empty($contractor_info)) {
			$this->data['contractor_name'] = $contractor_info['contractor_name'];
		} else {	
			$this->data['contractor_name'] = '';
		}

		if (isset($this->request->post['contractor_code'])) {
			$this->data['contractor_code'] = $this->request->post['contractor_code'];
		} elseif (!empty($contractor_info)) {
			$this->data['contractor_code'] = $contractor_info['contractor_code'];
		} else {	
			$this->data['contractor_code'] = '';
		}

		$unit_datas = $this->db->query("SELECT `unit`, `unit_id` FROM `oc_unit` GROUP BY `unit`")->rows;
		$unit_data = array();
		foreach ($unit_datas as $dkey => $dvalue) {
				$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;
		$this->template = 'catalog/contractor_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		if(isset($this->request->get['contractor_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/contractor')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/contractor')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if ((utf8_strlen($this->request->post['contractor_name']) < 1) || (utf8_strlen($this->request->post['contractor_name']) > 64)) {
			$this->error['contractor_name'] = $this->language->get('Contractor Name is Required');
		}

		if ((utf8_strlen($this->request->post['contractor_code']) < 1) || (utf8_strlen($this->request->post['contractor_code']) > 64)) {
			$this->error['contractor_code'] = 'Contractor Code is Required';
		} else {
			if(isset($this->request->get['contractor_id'])){
				$is_exist = $this->db->query("SELECT `contractor_id` FROM `oc_contractor` WHERE `contractor_id` <> '".$this->request->get['contractor_id']."' AND `contractor_code` = '".$this->request->post['contractor_code']."' ");
				if($is_exist->num_rows > 0){
					$this->error['contractor_code'] = 'Contractor Code Already Exist';
				}
			} else {
				$is_exist = $this->db->query("SELECT `contractor_id` FROM `oc_contractor` WHERE `contractor_code` = '".$this->request->post['contractor_code']."' ");
				if($is_exist->num_rows > 0){
					$this->error['contractor_code'] = 'Contractor Code Already Exist';
				}
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/contractor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		//echo '<pre>';print_r($this->request->get);exit;
		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $contractor_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByshiftId($contractor_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['contractor_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByshiftId($this->request->get['contractor_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/contractor');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_contractor->getContractor_tots($data);

			foreach ($results as $result) {
				$json[] = array(
					'contractor_id' => $result['contractor_id'], 
					'contractor_code' => $result['contractor_code'], 
					'name'            => strip_tags(html_entity_decode($result['contractor_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function export() {
		$this->language->load('catalog/contractor');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/contractor');
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'sort'  => $sort,
			'order' => $order,
		);
		$results = $this->model_catalog_contractor->getContractor_tots($data);
		$final_datas = array();
		foreach ($results as $result) {
			$unit_datas = $this->db->query("SELECT `unit` FROM `oc_contractor_location` WHERE `contractor_id` = '".$result['contractor_id']."' ")->rows;
			$unit_name_string = '';
			foreach($unit_datas as $ukey => $uvalue){
				$unit_name_string .= $uvalue['unit'].', ';
			}
			$unit_name_string = rtrim($unit_name_string, ', ');
			$final_datas[] = array(
				'contractor_id' => $result['contractor_id'],
				'name'          => $result['contractor_name'],
				'code'          => $result['contractor_code'],
				'unit'          => $unit_name_string,
			);
		}
		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		$template = new Template();		
		$template->data['final_datas'] = $final_datas;
		$template->data['title'] = 'Contractor';
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$template->data['base'] = HTTPS_SERVER;
		} else {
			$template->data['base'] = HTTP_SERVER;
		}
		$html = $template->fetch('catalog/contractor_html.tpl');
		//echo $html;exit;
		$filename = "Contractor";
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false);
		echo $html;
		exit;
	}	
}
?>