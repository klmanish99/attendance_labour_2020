<?php    
class ControllerCatalogWeek extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/week');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/week');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/week');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/week');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_week->addweek($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
public function export() { //echo "string";// exit;
		$this->language->load('catalog/week');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/week');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'week_id';
			$week_datas = $this->model_catalog_week->getweeks($data);
		//	echo "<pre>"; print_r($week_datas); exit;
			$final_datas = array();
			foreach($week_datas as $skey => $svalue){
				$final_datas[$skey]['week_name'] = $svalue['name'];
				$final_datas[$skey]['week_date'] = $svalue['date'];
				//$final_datas[$skey]['shift_outtime'] = $svalue['out_time'];

				$units = $this->db->query("SELECT `unit` from oc_unit");
				//echo "<pre>"; print_r($units->rows);exit;
				foreach ($units->rows as $ukey => $uvalue) {
					//echo "<pre>"; print_r($uvalue);
					$dept_datas = $this->db->query("SELECT * from oc_week_loc WHERE `location` = '". $uvalue['unit']."' AND `week_id` = '". $svalue['week_id']."'");
					if ($dept_datas->num_rows>0) {
						//echo "<pre>"; print_r($dept_datas->rows);//exit;
						foreach ($dept_datas->rows as $key => $value) {
							$unit_name=$value['location'];
							$unit_dept= unserialize($value['value']);
							$dept_unit_data = '';
						
							if($unit_dept){
								foreach ($unit_dept as $dkey => $dvalue) {
									$dept_unit_data .= $dvalue . ', '; 
								}

							}
							$final_datas[$skey]['unit_data'][$unit_name] =$dept_unit_data;
							//$unit_dept_data[$unit_name][]=$dept_unit_data;
						}
					} else {
							$final_datas[$skey]['unit_data'][$uvalue['unit']] ="";
					}
				}
				
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Weeks Schedule';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/weekly_off_html.tpl');
			//echo $html;exit;
			$filename = "week_schedule";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	public function update() {
		$this->language->load('catalog/week');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/week');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_week->editweek($this->request->get['week_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/week');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/week');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $week_id) {
				$this->model_catalog_week->deleteweek($week_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['week_id']) && $this->validateDelete()){
			$this->model_catalog_week->deleteweek($this->request->get['week_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/week/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/week/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export'] = $this->url->link('catalog/week/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['weeks'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$week_total = $this->model_catalog_week->getTotalweeks($data);

		$results = $this->model_catalog_week->getweeks($data);

		// echo '<pre>';
		// print_r($results);
		// exit;

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/week/update', 'token=' . $this->session->data['token'] . '&week_id=' . $result['week_id'] . $url, 'SSL')
			);

			// $action[] = array(
			// 	'text' => 'Delete',
			// 	'href' => $this->url->link('catalog/week/delete', 'token=' . $this->session->data['token'] . '&week_id=' . $result['week_id'] . $url, 'SSL')
			// );

			$this->data['weeks'][] = array(
				'week_id' => $result['week_id'],
				'name' => $result['name'],
				'date'            => $result['date'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['week_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/week', 'token=' . $this->session->data['token'] . '&sort=date' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $week_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		
		$this->template = 'catalog/week_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['date'])) {
			$this->data['error_date'] = $this->error['date'];
		} else {
			$this->data['error_date'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['week_id'])) {
			$this->data['action'] = $this->url->link('catalog/week/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/week/update', 'token=' . $this->session->data['token'] . '&week_id=' . $this->request->get['week_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/week', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['week_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$week_info = $this->model_catalog_week->getweek($this->request->get['week_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('report/attendance');

		//$unit_data = array(
		//	'Mumbai' => 'Mumbai',
		//	'Pune' => 'Pune',
		//	'Moving' => 'Moving' 
		//);

		//$this->data['unit_data'] = $unit_data;
		
		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		foreach ($department_datas as $dkey => $dvalue) {
			$dvalue['d_name'] = html_entity_decode($dvalue['d_name']);
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;

		$this->load->model('catalog/division');
		$division_datas = $this->model_catalog_division->getDivisions($data = array());
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$dvalue['division'] = html_entity_decode($dvalue['division']);
					$division_data[$dvalue['division_id']]['name'] = $dvalue['division'];
					$division_data[$dvalue['division_id']]['id'] = $dvalue['division_id'];
				}
			} else {
				$dvalue['division'] = html_entity_decode($dvalue['division']);
				$division_data[$dvalue['division_id']]['name'] = $dvalue['division'];
				$division_data[$dvalue['division_id']]['id'] = $dvalue['division_id'];
			}
		}
		$this->data['division_data'] = $division_data;

		$unit_datas = $this->db->query("SELECT `unit`, `unit_id`, `division_id` FROM `oc_unit` GROUP BY `unit`")->rows;
		$unit_data = array();
		//$department_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$unit_data[$dvalue['unit_id']]['name'] = $dvalue['unit'];
					$unit_data[$dvalue['unit_id']]['id'] = $dvalue['unit_id'];
					$unit_data[$dvalue['unit_id']]['division_id'] = $dvalue['division_id'];
				}
			} else {
				$unit_data[$dvalue['unit_id']]['name'] = $dvalue['unit'];
				$unit_data[$dvalue['unit_id']]['id'] = $dvalue['unit_id'];
				$unit_data[$dvalue['unit_id']]['division_id'] = $dvalue['division_id'];
			}
		}
		$this->data['unit_data'] = $unit_data;

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($week_info)) {
			$this->data['name'] = $week_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['date'])) {
			$this->data['date'] = $this->request->post['date'];
		} elseif (!empty($week_info)) {
			$this->data['date'] = $week_info['date'];
		} else {	
			$this->data['date'] = '';
		}

		if (isset($this->request->post['week_datas'])) {
			$this->data['week_datas'] = $this->request->post['week_datas'];
		} elseif (!empty($week_info['week_id'])) {
			$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			foreach ($units as $key => $unit) {
				$unit_values = $this->db->query("SELECT * FROM `oc_week_loc` where week_id = '".$this->request->get['week_id']."' AND location_id = '".$unit['unit_id']."' ");
				if ($unit_values->num_rows > 0) { 
					$this->data['week_datas'][$unit['unit_id']] =  unserialize($unit_values->row['value']);
					foreach($this->data['week_datas'][$unit['unit_id']] as $key => $value){
						$this->data['week_datas'][$unit['unit_id']][$key] = html_entity_decode(strtolower(trim($value)));
					}
				} else {
					$this->data['week_datas'][$unit['unit_id']] = array();
				}
			}
			// echo "<pre>";
			// print_r($this->data['unit_shift']);exit;
		} else {
			$units = $this->db->query("SELECT * FROM `oc_unit`")->rows;
			foreach ($units as $key => $unit) {
				$this->data['week_datas'][$unit['unit_id']] = array();
			}
		}

		if (isset($this->request->post['week_division_datas'])) {
			$this->data['week_division_datas'] = $this->request->post['week_division_datas'];
		} elseif (!empty($week_info['week_id'])) {
			if($week_info['division_datas'] != ''){
				$this->data['week_division_datas'] = unserialize($week_info['division_datas']);
				foreach($this->data['week_division_datas'] as $key => $value){
					$this->data['week_division_datas'][$key] = html_entity_decode(strtolower(trim($value)));
				}
			} else {
				$this->data['week_division_datas'] = array();	
			}
			// echo "<pre>";
			// print_r($this->data['week_division_datas']);
			// exit;
		} else {
			$this->data['week_division_datas'] = array();
		}

		$this->template = 'catalog/week_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/halfday');

		if(isset($this->request->get['week_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/week')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/week')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = 'Please Enter Weekly Off Name';//$this->language->get('error_name');
		}

		if($this->request->post['date'] != '' && !isset($this->request->get['week_id'])){
			$is_exist = $this->model_catalog_halfday->getweek_exist($this->request->post['date']);
			if($is_exist == 1){
				$this->error['warning'] = 'Weekly Off With Same Date Present';
			}
		}

		// $date = $this->request->post['date'];
		// if($this->user->getId() == 3){
		// 	if(isset($this->request->post['dept_holiday_mumbai'])){
		// 		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `day_close_status` = '1' AND LOWER(`unit`) = 'mumbai' ";
		// 		$query = $this->db->query($sql);
		// 		if($query->num_rows > 0) {
		// 			$this->error['warning'] = 'Day Closed for Mumbai on date : ' . $this->request->post['date'];		
		// 		}
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/week')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $week_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByweekId($week_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['week_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByweekId($this->request->get['week_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/week');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_week->getweeks($data);

			foreach ($results as $result) {
				$json[] = array(
					'week_id' => $result['week_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}	
}
?>