<?php
class ControllerCatalogDesignation extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/designation');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/designation');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/designation');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/designation');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_designation->addDesignation($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/designation');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/designation');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_designation->editDesignation($this->request->get['designation_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function export() { //echo "string";exit;
		$this->language->load('catalog/designation');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/designation');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'designation_id';
			$designation_datas = $this->model_catalog_designation->getDesignations($data);

			$final_datas = array();
			foreach($designation_datas as $skey => $svalue){
				$final_datas[$skey]['name'] = $svalue['d_name'];
				$final_datas[$skey]['code'] = $svalue['d_code'];
				$final_datas[$skey]['grade_name'] = $svalue['grade_name'];
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Designation';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/designation_html.tpl');
			//echo $html;exit;
			$filename = "Designation";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	public function delete() {
		$this->language->load('catalog/designation');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/designation');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $designation_id) {
				$this->model_catalog_designation->deleteDesignation($designation_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_grade'])) {
			$filter_grade = $this->request->get['filter_grade'];
		} else {
			$filter_grade = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'd_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_grade'])) {
			$url .= '&filter_grade=' . $this->request->get['filter_grade'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/designation/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/designation/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export'] = $this->url->link('catalog/designation/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['designations'] = array();

		$data = array(
			'filter_name'  => $filter_name,
			'filter_name_id'  => $filter_name_id,
			'filter_grade'  => $filter_grade,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$designation_total = $this->model_catalog_designation->getTotalDesignations($data);

		$results = $this->model_catalog_designation->getDesignations($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/designation/update', 'token=' . $this->session->data['token'] . '&designation_id=' . $result['designation_id'] . $url, 'SSL')
			);

			$this->data['designations'][] = array(
				'designation_id' => $result['designation_id'],
				'd_name'          => $result['d_name'],
				'd_code'          => $result['d_code'],
				'grade_name'          => $result['grade_name'],
				'selected'       => isset($this->request->post['selected']) && in_array($result['designation_id'], $this->request->post['selected']),
				'action'         => $action
			);
		}	

		$this->load->model('catalog/grade');
		$grade_datas = $this->model_catalog_grade->getGrades();
		$grade_data = array();
		$grade_data['0'] = 'All';
		foreach ($grade_datas as $dkey => $dvalue) {
			$grade_data[$dvalue['grade_id']] = $dvalue['g_name'];
		}
		$this->data['grade_data'] = $grade_data;

		$this->data['token'] = $this->session->data['token'];

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_grade'])) {
			$url .= '&filter_grade=' . $this->request->get['filter_grade'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_d_name'] = $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . '&sort=d_name' . $url, 'SSL');
		$this->data['sort_d_code'] = $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . '&sort=d_code' . $url, 'SSL');
		$this->data['sort_grade'] = $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . '&sort=grade_name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_grade'])) {
			$url .= '&filter_grade=' . $this->request->get['filter_grade'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $designation_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_grade'] = $filter_grade;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/designation_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_bottom'] = $this->language->get('entry_bottom');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_grade'])) {
			$url .= '&filter_grade=' . $this->request->get['filter_grade'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['designation_id'])) {
			$this->data['action'] = $this->url->link('catalog/designation/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/designation/update', 'token=' . $this->session->data['token'] . '&designation_id=' . $this->request->get['designation_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/designation', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['designation_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$designation_info = $this->model_catalog_designation->getDesignation($this->request->get['designation_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['d_name'])) {
			$this->data['d_name'] = $this->request->post['d_name'];
		} elseif (!empty($designation_info)) {
			$this->data['d_name'] = $designation_info['d_name'];
		} else {
			$this->data['d_name'] = '';
		}

		if (isset($this->request->post['d_code'])) {
			$this->data['d_code'] = $this->request->post['d_code'];
		} elseif (!empty($designation_info)) {
			$this->data['d_code'] = $designation_info['d_code'];
		} else {
			$this->data['d_code'] = '';
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($designation_info)) {
			$this->data['status'] = $designation_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['grade_id'])) {
			$this->data['grade_id'] = $this->request->post['grade_id'];
		} elseif (!empty($designation_info)) {
			$this->data['grade_id'] = $designation_info['grade_id'];
		} else {
			$this->data['grade_id'] = '';
		}

		if (isset($this->request->post['grade_name'])) {
			$this->data['grade_name'] = $this->request->post['grade_name'];
		} elseif (!empty($designation_info)) {
			$this->data['grade_name'] = $designation_info['grade_name'];
		} else {
			$this->data['grade_name'] = '';
		}

		$this->load->model('catalog/grade');
		$grade_datas = $this->model_catalog_grade->getGrades();
		$grade_data = array();
		$grade_data['0'] = 'Please Select';
		foreach ($grade_datas as $dkey => $dvalue) {
			$grade_data[$dvalue['grade_id']] = $dvalue['g_name'];
		}
		$this->data['grade_data'] = $grade_data;

		$this->template = 'catalog/designation_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if(isset($this->request->get['designation_id'])){
			if (!$this->user->hasPermission('modify', 'catalog/designation')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		} else {
			if (!$this->user->hasPermission('add', 'catalog/designation')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
		}

		if ((utf8_strlen($this->request->post['d_name']) < 1) || (utf8_strlen($this->request->post['d_name']) > 64)) {
			$this->error['title'] = 'Plese Enter Designation Name';
		} else {
			if(isset($this->request->get['designation_id'])){
				$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `designation_id` <> '".$this->request->get['designation_id']."' AND `d_name` = '".$this->request->post['d_name']."' ");
				if($is_exist->num_rows > 0){
					$this->error['title'] = 'Designation Name Already Exist';
				}
			} else {
				$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->request->post['d_name']."' ");
				if($is_exist->num_rows > 0){
					$this->error['title'] = 'Designation Name Already Exist';
				}
			}
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('delete', 'catalog/designation')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/designation');
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_designation->getDesignations($data);
			foreach ($results as $result) {
				$json[] = array(
					'designation_id' => $result['designation_id'],
					'd_name'            => strip_tags(html_entity_decode($result['d_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}
		$sort_order = array();
		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['d_name'];
		}
		array_multisort($sort_order, SORT_ASC, $json);
		$this->response->setOutput(json_encode($json));
	}
}
?>