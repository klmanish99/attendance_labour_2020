<?php    
class ControllerTransactionRequestform extends Controller { 
	private $error = array();

	public function index() {

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		$this->language->load('transaction/requestform');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function insert() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$data = $this->request->post;
			
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
			if(isset($emp_data['emp_code'])){
				$dept_name = $emp_data['department'];
				$unit_name = $emp_data['unit'];
			} else {
				$dept_name = '';
				$unit_name = '';
			}
			
			$sql = "INSERT INTO `oc_requestform` SET 
				`emp_id` = '".$data['e_name_id']."', 
				`dot` = '".$data['dot']."',
				`doi` = '".date('Y-m-d')."',
				`batch_id` = '".$data['batch_id']."',
				`request_type` = '".$data['request_type']."',
				`description` = '".$this->db->escape($data['description'])."',
				`dept_name` = '".$this->db->escape($dept_name)."',
				`unit` = '".$this->db->escape($unit_name)."',
				`approval_1` = '0' ";
			//echo $sql;
			//echo '<br />';
			$this->db->query($sql);

			//exit;
			$this->session->data['success'] = 'You have Inserted the Request Entry';	
			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_date_proc'])) {
				$url .= '&filter_date_proc=' . $this->request->get['filter_date_proc'];
			}

			$this->redirect($this->url->link('transaction/requestform', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$can_leave_sql = "DELETE FROM `oc_requestform` WHERE `id` = '".$id."' ";
				$this->db->query($can_leave_sql);
			}

			$this->session->data['success'] = 'Request Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_date_proc'])) {
				$url .= '&filter_date_proc=' . $this->request->get['filter_date_proc'];
			}

			$this->redirect($this->url->link('transaction/requestform', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['id']) && $this->validateDelete()){
			$id = $this->request->get['id'];
			$can_leave_sql = "DELETE FROM `oc_requestform` WHERE `id` = '".$id."' ";
			$this->db->query($can_leave_sql);
			$this->session->data['success'] = 'Request Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_date_proc'])) {
				$url .= '&filter_date_proc=' . $this->request->get['filter_date_proc'];
			}

			$this->redirect($this->url->link('transaction/requestform', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} elseif(isset($this->session->data['d_emp_id'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['d_emp_id']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} elseif(isset($this->session->data['d_emp_id'])){
			$filter_name_id = $this->session->data['d_emp_id'];
		} else {
			$filter_name_id = '';
		}

		if($filter_name_id != ''){
			$filter_dept = $this->db->query("SELECT `department` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['department'];
		} elseif($filter_name_id_1 != ''){
			$filter_dept = $this->db->query("SELECT `department` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id_1."' ")->row['department'];
		} else {
			$filter_dept = '';
		}

		if($filter_name_id != ''){
			$filter_unit = $this->db->query("SELECT `unit` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['unit'];
		} elseif($filter_name_id_1 != ''){
			$filter_unit = $this->db->query("SELECT `unit` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id_1."' ")->row['unit'];
		} else {
			$filter_unit = '';
		}

		//echo $filter_unit;exit;

		if (isset($this->request->get['filter_approval_1'])) {
			$filter_approval_1 = $this->request->get['filter_approval_1'];
		} else {
			$filter_approval_1 = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['filter_date_proc'])) {
			$filter_date_proc = $this->request->get['filter_date_proc'];
		} else {
			$filter_date_proc = null;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_proc'])) {
			$url .= '&filter_date_proc=' . $this->request->get['filter_date_proc'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/requestform', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$url_insert = '&filter_name_id='.$filter_name_id.'&filter_name='.$filter_name;

		$this->data['insert'] = $this->url->link('transaction/requestform/insert', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['delete'] = $this->url->link('transaction/requestform/delete', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		
		$this->data['requests'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_approval_1' => $filter_approval_1,
			'filter_date' => $filter_date,
			'filter_date_proc' => $filter_date_proc,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$request_types = array(
			'1' => 'Revert Leave',
			'2' => 'Missing Punch',
			'3' => 'Shift Change',
			'4' => 'Weekly Off Change',
			'5' => 'Holiday Change',
		);

		
		$employee_total = $this->model_transaction_transaction->getTotalrequest($data);
		$results = $this->model_transaction_transaction->getrequests($data);
		foreach ($results as $result) {
			$action = array();
			if ($result['approval_1'] == '0') {
				$action[] = array(
					'text' => 'Delete',
					'href' => $this->url->link('transaction/requestform/delete', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
				);
			} else {
				$action[] = array(
					'text' => '---',
					'href' => ''
				);
			}	
			
			//$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

			if($result['approval_1'] == '0'){
				$approval_1 = 'Pending';
			} elseif($result['approval_1'] == '1' || $result['approval_1'] == '3' || $result['approval_1'] == '4') {
				$approval_1 = 'Approved';
			} elseif($result['approval_1'] == '2'){
				$approval_1 = 'Rejected';
			}

			if($result['batch_id'] != '0'){
				$leave_from = $this->model_transaction_transaction->getleave_from_ess($result['batch_id']);
				$leave_to = $this->model_transaction_transaction->getleave_to_ess($result['batch_id']);
				$leave_data = $this->model_transaction_transaction->getleave_data($result['batch_id']);
				$leave_data = $leave_data['leave_type'].' '.date('d-m-Y', strtotime($leave_from)).' - '.date('d-m-Y', strtotime($leave_to));
				$result['dot'] = $leave_data;
			} else {
				$leave_data = '';
			}
			$this->data['requests'][] = array(
				'id' => $result['id'],
				'dot' => $result['dot'],
				'doi' => $result['doi'],
				'description' => $result['description'],
				'request_type' => $request_types[$result['request_type']],
				'leave_data'        => $leave_data,
				'approval_1' => $approval_1,
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;
		$approves = array(
			'0' => 'All',
			'1' => 'Pending',
			'2' => 'Approved',
			'3' => 'Rejected',
		);

		$this->data['approves'] = $approves;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_proc'])) {
			$url .= '&filter_date_proc=' . $this->request->get['filter_date_proc'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}
		

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/requestform', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_date_proc'] = $filter_date_proc;
		$this->data['filter_approval_1'] = $filter_approval_1;
		$this->data['filter_dept'] = $filter_dept;
		$this->data['filter_unit'] = $filter_unit;
		
		$this->template = 'transaction/requestform_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/requestform/getForm', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		
		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_date_proc'])) {
			$url .= '&filter_date_proc=' . $this->request->get['filter_date_proc'];
		}

		$this->data['cancel'] = $this->url->link('transaction/requestform', 'token=' . $this->session->data['token'].$url, 'SSL');
		$this->data['action'] = $this->url->link('transaction/requestform/insert', 'token=' . $this->session->data['token'].$url, 'SSL');

		
		$transaction_data = array();
		$emp_data = array();
		if (isset($this->request->get['batch_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->getleave_transaction_data_ess($this->request->get['batch_id']);
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id']);
		} elseif(isset($this->session->data['emp_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->session->data['emp_code']);
		} elseif(isset($this->session->data['d_emp_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->session->data['d_emp_id']);
		}

		$this->data['request_types'] = array(
			'0' => 'Please Select',
			'1' => 'Revert Leave',
			'2' => 'Missing Punch',
			'3' => 'Shift Change',
			'4' => 'Weekly Off Change',
			'5' => 'Holiday Change',
			//'6' => 'On Duty',
		);

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['batch_id'])) {
			$this->data['batch_id'] = $this->request->post['batch_id'];
		} elseif(isset($transaction_data[0]['batch_id'])) {
			$this->data['batch_id'] = $transaction_data[0]['batch_id'];
		} else {	
			$this->data['batch_id'] = 0;
		}

		if($this->data['batch_id'] == 0){
			$this->data['is_leave'] = '0';
		} else {
			$this->data['is_leave'] = '1';
		}

		if (isset($this->request->post['e_name_id'])) {
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['e_name'] = $this->request->post['e_name'];
		} elseif(isset($transaction_data[0]['emp_id'])) {
			$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
			$this->data['e_name'] = $emp_data['name'];
		} elseif(isset($emp_data['emp_code'])) {
			$this->data['e_name_id'] = $emp_data['emp_code'];
			$this->data['e_name'] = $emp_data['name'];
		} else {	
			$this->data['e_name_id'] = '0';
			$this->data['e_name'] = '0';
		}

		if (isset($this->request->post['leave_data'])) {
			$this->data['leave_data'] = $this->request->post['leave_data'];
		} elseif(isset($transaction_data[0]['emp_id'])) {
			$leave_from = $this->model_transaction_transaction->getleave_from_ess($transaction_data[0]['batch_id']);
			$leave_to = $this->model_transaction_transaction->getleave_to_ess($transaction_data[0]['batch_id']);
			$leave_type = $transaction_data[0]['leave_type'];
			$leave_data = $leave_type.' '.date('d-m-Y', strtotime($leave_from)).' - '.date('d-m-Y', strtotime($leave_to));
			$this->data['leave_data'] = $leave_data;
		} else {	
			$this->data['leave_data'] = '';
		}

		if (isset($this->request->post['request_type'])) {
			$this->data['request_type'] = $this->request->post['request_type'];
		} elseif($this->data['is_leave'] == '1'){
			$this->data['request_type'] = '1';
		} else {	
			$this->data['request_type'] = '0';//date('d-m-Y');
		}

		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} else {	
			$this->data['description'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['dot'])) {
			$this->data['dot'] = $this->request->post['dot'];
		} else {	
			$this->data['dot'] = date('d-m-Y');
		}

		// echo '<pre>';
		// print_r($this->data);
		// exit;

		$this->template = 'transaction/requestform_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	} 

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	} 

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		// if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete(){
		// $this->load->model('transaction/transaction');
		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $batch_id) {
		// 		$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 		$date_res = $this->db->query($leave_sql);
		// 		if ($date_res->num_rows > 0) {
		// 			$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 		}
		// 	}	
		// } elseif(isset($this->request->get['batch_id'])){
		// 	$batch_id = $this->request->get['batch_id'];
		// 	$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 	$date_res = $this->db->query($leave_sql);
		// 	if ($date_res->num_rows > 0) {
		// 		$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
