<?php
class ControllerTransactionTransaction extends Controller { 
	public function index() {  
		$this->language->load('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['generate'] = $this->url->link('transaction/transaction/generate', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->template = 'transaction/transaction.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function generate(){
		$this->language->load('transaction/transaction');
		$this->document->setTitle($this->language->get('heading_title'));			
		
		$this->load->model('transaction/transaction');

		$raw_attendance_datass = $this->model_transaction_transaction->getrawattendance_group();
		//echo '<pre>';
		//print_r($raw_attendance_datass);
		// exit;
		if($raw_attendance_datass) {
			foreach ($raw_attendance_datass as $rkeyss => $rvaluess) {
				$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
				if(isset($emp_data['name']) && $emp_data['name'] != ''){
					$emp_name = $emp_data['name'];
					$department = $emp_data['department'];
					$unit = $emp_data['unit'];
					$group = $emp_data['group'];

					$shift_data = $this->model_transaction_transaction->getshiftdata($emp_data['shift']);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];

						$act_intime = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
						$act_outtime = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date']);
						
						//$act_intime = "09:15:00";
						//$act_outtime = "17:20:00";


						$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date1 = new DateTime($rvaluess['punch_date'].' '.$shift_outtime);
						$since_start1 = $start_date1->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
						$early_time = $since_start1->h.':'.$since_start1->i.':'.$since_start1->s;					
						if($since_start1->invert == 0){
							$early_time = '00:00:00';
						}					
						
						$start_date2 = new DateTime($rvaluess['punch_date'].' '.$act_intime);
						$since_start2 = $start_date2->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
						$working_time = $since_start2->h.':'.$since_start2->i.':'.$since_start2->s;


						$day = date('j', strtotime($rvaluess['punch_date']));
						$month = date('n', strtotime($rvaluess['punch_date']));
						$year = date('Y', strtotime($rvaluess['punch_date']));

						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `weekly_off` = '".$emp_data['weekly_off']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$rvaluess['punch_date']."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."' ";
						$this->db->query($sql);
						//echo $sql;
						//exit;
						// echo '<pre>';
						// print_r($act_intime);
						// echo '<pre>';
						// print_r($shift_intime);
						// echo '<pre>';
						// print_r($act_outtime);
						// echo '<pre>';
						// print_r($shift_outtime);
						// echo '<pre>';
						// print_r($late_time);
						// echo '<pre>';
						// print_r($early_time);
						// echo '<pre>';
						// print_r($working_time);
						// exit;
					}
				}
			}
			$update = "UPDATE `oc_attendance` SET `status` = '1' ";
			$this->db->query($update);
			//echo 'Done';exit;
			$this->session->data['success'] = 'Transaction Generated';
			$this->redirect($this->url->link('transaction/transaction', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$this->session->data['warning'] = 'No Data available';
			$this->redirect($this->url->link('transaction/transaction', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function generate_today(){
		date_default_timezone_set("Asia/Kolkata");
		if (isset($this->request->get['unit'])) {
			$filter_unit = $this->request->get['unit'];
		} else {
			$filter_unit = 0;
		}

		$this->db->query("start transaction;");
		
		$this->language->load('transaction/transaction');
		$this->document->setTitle($this->language->get('heading_title'));			
		
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');

		$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"SmartOffice", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
		     //echo "Connection established.<br />";
		} else {
		     //echo "Connection could not be established.<br />";
		     $dat['connection'] = 'Connection could not be established';
		     //echo '<pre>';
		     //print_r(sqlsrv_errors());
		     //exit;
		     //die( print_r( sqlsrv_errors(), true));
		}
		
		$exp_datas = array();
		$exp_datas_2 = array();
		$filter_date_start = date('Y-m-d');
		//$filter_date_start = '2016-04-09';
		//echo '<pre>';
		//print_r($exp_datas);
		// exit;
		// $table_name = "DeviceLogs_".date('n').'_'.date('Y');
		// $sql = "SELECT `LogDate`, `UserId` FROM ".$table_name." WHERE YEAR(LogDate) = '".date('Y')."' AND MONTH(LogDate) = '".date('m')."' AND DAY(LogDate) = '".date('d')."' ORDER BY TIME(`LogDate`) ASC";
		// $device_datas = $this->db1->query($sql);
		// $exp_datas = array();
		// if($device_datas->num_rows > 0){
		// 	foreach($device_datas->rows as $dkey => $dvalue){
		// 		$log_date = date('Y-m-d', strtotime($dvalue['LogDate']));
		// 		$log_time = date('H:i:s', strtotime($dvalue['LogDate']));
		// 		$exp_datas[$dkey]['user_id'] = $dvalue['UserId'];
		// 		$exp_datas[$dkey]['time'] = $log_time;
		// 	}
		// }

		$day = date('d', strtotime($filter_date_start));
		$month = date('m', strtotime($filter_date_start));
		$year = date('Y', strtotime($filter_date_start));
		//$sql = "SELECT * FROM ".$table_name." WHERE (DATEPART(yy, LogDate) = '".$year."' AND DATEPART(mm, LogDate) = '".$month."' AND DATEPART(dd, LogDate) = '".$day."')";
		$sql = "SELECT * FROM SmartOffice.dbo.Staffdevicelogsnew WHERE (DATEPART(yy, LogDate) = '".$year."' AND DATEPART(mm, LogDate) = '".$month."' AND DATEPART(dd, LogDate) = '".$day."')";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		if( $stmt === false) {
		   //echo'<pre>';
		   //print_r(sqlsrv_errors());
		   //exit;
		}
		
		$exp_datas = array();
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
		  	//$user_id = sprintf('%02d', $row['UserId']);
		  	//$device_id = sprintf('%02d', $row['DeviceId']);
		  	$user_id = $row['UserId'];
		  	$device_id = $row['DeviceId'];
			$log_id = $row['DeviceLogId'];
		 	$dates = $row['LogDate'];
		 	$date = $dates->format("Y-m-d");
		 	$times = $row['LogDate'];
		 	$time = $times->format("H:i:s");
		 	$download_dates = $row['DownloadDate'];
		 	$download_date = $download_dates->format("Y-m-d");
		 	$exp_datas[] = array(
		 		'user_id' => $user_id,
				'log_id' => $log_id,
		 		'download_date' => $download_date,
		 		'date' => $date,
		 		'time' => $time,
		 		'device_id' => $device_id,
		 	);
		}
	
		//echo '<pre>';
		//print_r($exp_datas);
		//exit;

		$this->data['employees'] = array();
		$employees = array();
		$this->load->model('catalog/employee');		
				
		foreach($exp_datas as $data) {
			//$tdata = trim($data);
			$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
			$in_time = $data['time'];//substr($tdata, 6, 5);
			$device_id = $data['device_id'];//substr($tdata, 6, 5);
			$date = $data['date'];//substr($tdata, 6, 5);
			$download_date = $data['download_date'];//substr($tdata, 6, 5);
			$log_id = $data['log_id'];//substr($tdata, 6, 5);
			if($emp_id != '') {
				$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
				if(isset($result['emp_code'])){
					$employees[] = array(
						'employee_id' => $result['emp_code'],
						'emp_name' => $result['name'],
						'department' => $result['department'],
						'department_id' => $result['department_id'],
						'unit' => $result['unit'],
						'unit_id' => $result['unit_id'],
						'division' => $result['division'],
						'division_id' => $result['division_id'],
						'region' => $result['region'],
						'region_id' => $result['region_id'],
						'company' => $result['company'],
						'company_id' => $result['company_id'],
						'group' => $result['group'],
						'in_time' => $in_time,
						'device_id' => $device_id,
						'download_date' => $download_date,
						'log_id' => $log_id,
						'punch_date' => $date,
						'fdate' => date('Y-m-d H:i:s', strtotime($date.' '.$in_time))
					);
				}
			}
		}
		usort($employees, array($this, "sortByOrder"));
		$o_emp = array();
		foreach ($employees as $ekey => $evalue) {
			if(!isset($o_emp[$evalue['employee_id']])){
				$o_emp[$evalue['employee_id']] = $evalue['employee_id'];
				$this->data['employees'][] = $evalue;
			} 
		}
		// echo '<pre>';
		// print_r($o_emp);
		// echo '<pre>';
		// print_r($this->data['employees']);
		// exit;
		if(isset($this->data['employees']) && count($this->data['employees']) > 0){
			$this->model_transaction_transaction->deleteorderhistory_new($filter_date_start);
			foreach ($this->data['employees'] as $fkey => $employee) {
				
				$mystring = $employee['in_time'];
				$findme   = ':';
				$pos = strpos($mystring, $findme);
				if($pos !== false){
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
				} else {
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
				}
				if($in_times != ':'){
					$in_time = date("h:i:s", strtotime($in_times));
				} else {
					$in_time = '';
				}
				$employee['in_time'] = $in_times;
				
				$day_date = date('j');
				$month = date('n');
				$year = date('Y');

				$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$employee['employee_id']."' ";
				//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
				$shift_schedule = $this->db->query($update3)->row;
				$schedule_raw = explode('_', $shift_schedule[$day_date]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2] = 1;
				}
				// echo '<pre>';
				// print_r($schedule_raw);
				// exit;	
				$shift_intime = '00:00:00';
				if($schedule_raw[0] == 'S'){
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
					}
				} elseif($schedule_raw[0] == 'W' || $schedule_raw[0] == 'H' || $schedule_raw[0] == 'HD' || $schedule_raw[0] == 'C'){
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
					}
				}
				if($shift_data['shift_id'] == '1'){
					$shift_intime_plus_one = Date('H:i:s', strtotime($shift_intime .' +30 minutes'));
					$start_date = new DateTime($filter_date_start.' '.$shift_intime_plus_one);
					$since_start = $start_date->diff(new DateTime($filter_date_start.' '.$in_times));
					if($since_start->h > 0){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					} else {
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					}
				} else {
					$start_date = new DateTime($filter_date_start.' '.$shift_intime);
					$since_start = $start_date->diff(new DateTime($filter_date_start.' '.$in_times));
					/*
					if($since_start->h > 0){
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					} else {
						$late_hour = $since_start->h;
						$late_min = $since_start->i;
						$late_sec = $since_start->s;
					}
					*/
					$late_hour = 0;
					$late_min = 0;
					$late_sec = 0;
				}
				$late_time = sprintf("%02d", $late_hour).':'.sprintf("%02d", $late_min).':'.sprintf("%02d", $late_sec);
				if($since_start->invert == 1){
					$late_time = '00:00:00';
				} 
				$employee['shift_intime'] = $shift_intime;
				$employee['late_time'] = $late_time;
				// echo '<pre>';
				// print_r($employee);
				// exit;
				$this->model_transaction_transaction->insert_attendance_data_new($employee);
			}
		}

		$this->db->query("commit;");
		$this->session->data['success'] = 'Transaction Generated';
		$url = '&filter_date_start='.$filter_date_start;
		if($filter_unit){
			$url .= '&unit='.$filter_unit;
		}
		$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'].$url, 'SSL'));
		
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
	   	$v2 = strtotime($b['fdate']);
	   	return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
	 		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	 	//}
	 	//return $a['punch_date'] - $b['punch_date'];
	}

	public function generate_filter_date(){
		// $referrerss = explode('route=', $this->request->server['HTTP_REFERER']);
		// $referrers = explode('&', $referrerss[1]);
		// $referrer = $referrers[0]; 
		// echo '<pre>';
		// print_r($referrer);
		// exit;

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		if (isset($this->request->get['unit'])) {
			$filter_unit = $this->request->get['unit'];
		} else {
			$filter_unit = 0;
		}

		$this->db->query("start transaction;");

		$filter_date_start = '';
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		}

		$this->language->load('transaction/transaction');
		$this->document->setTitle($this->language->get('heading_title'));			
		
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');

		//echo $filter_date_start;exit;

		if($filter_date_start != '2016-11-21'){
			$prev_day = date('Y-m-d', strtotime($filter_date_start .' -1 day'));
			$update2 = "SELECT * FROM `oc_transaction` WHERE `date` = '".$prev_day."' AND `day_close_status` = '1' ";
			$day_status = $this->db->query($update2);
			if($day_status->num_rows == 0){
				$this->db->query("commit;");
				$this->session->data['warning'] = 'Please Close Previous Day';
				if(isset($this->request->get['home'])){
					$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
				} else {
					$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'], 'SSL'));
				}
			}
		}


		$update2 = "SELECT * FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND `day_close_status` = '1' ";
		if($filter_unit){
			$update2 .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
		}
		$day_status = $this->db->query($update2);
		// echo '<pre>';
		// print_r($day_status);
		// exit;
		if($day_status->num_rows == 0){
			$exp_datas = array();
			$exp_datas_2 = array();
			
			$table_name = "DeviceLogs_".date('n', strtotime($filter_date_start)).'_'.date('Y', strtotime($filter_date_start));
			$sql = "SELECT `LogDate`, `UserId` FROM ".$table_name." WHERE YEAR(LogDate) = '".date('Y', strtotime($filter_date_start))."' AND MONTH(LogDate) = '".date('m', strtotime($filter_date_start))."' AND DAY(LogDate) = '".date('d', strtotime($filter_date_start))."' ORDER BY TIME(`LogDate`) ASC";
			$device_datas = $this->db1->query($sql);
			$exp_datas = array();
			if($device_datas->num_rows > 0){
				foreach($device_datas->rows as $dkey => $dvalue){
					$log_date = date('Y-m-d', strtotime($dvalue['LogDate']));
					$log_time = date('H:i:s', strtotime($dvalue['LogDate']));
					$exp_datas[$dkey]['user_id'] = $dvalue['UserId'];
					$exp_datas[$dkey]['time'] = $log_time;
				}
			}

			$this->data['employees'] = array();
			$employees = array();
			$this->load->model('catalog/employee');		
			/* commented as array_revere used to revert the array values and then ids for punch time are reversed */
			//$rev_exp_datas = array_reverse($exp_datas);
			//echo '<pre>';
			//print_r($rev_exp_datas);
			//exit;
			foreach($exp_datas as $data) {
				//$tdata = trim($data);
				$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
				$in_time = $data['time'];//substr($tdata, 6, 5);
				if($emp_id != '') {
					$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
					if(isset($result['emp_code'])){
						$employees[] = array(
							'employee_id' => $result['emp_code'],
							'card_id' => $result['card_number'],
							'in_time' => $in_time,
							'punch_date' => $filter_date_start,
							'fdate' => date('Y-m-d H:i:s', strtotime($filter_date_start.' '.$in_time))
						);
					}
				}
			}

			// echo '<pre>';
			// print_r($employees);
			// exit;

			//usort($employees, array($this, "sortByOrder"));
			$exp_datas = array();
			$next_date = date('Y-m-d', strtotime($filter_date_start .' +1 day'));
			
			$table_name = "DeviceLogs_".date('n', strtotime($next_date)).'_'.date('Y', strtotime($next_date));
			$sql = "SELECT `LogDate`, `UserId` FROM ".$table_name." WHERE YEAR(LogDate) = '".date('Y', strtotime($next_date))."' AND MONTH(LogDate) = '".date('m', strtotime($next_date))."' AND DAY(LogDate) = '".date('d', strtotime($next_date))."' ORDER BY TIME(`LogDate`) ASC";
			$device_datas = $this->db1->query($sql);
			$exp_datas = array();
			if($device_datas->num_rows > 0){
				foreach($device_datas->rows as $dkey => $dvalue){
					$log_date = date('Y-m-d', strtotime($dvalue['LogDate']));
					$log_time = date('H:i:s', strtotime($dvalue['LogDate']));
					$exp_datas[$dkey]['user_id'] = $dvalue['UserId'];
					$exp_datas[$dkey]['time'] = $log_time;
				}
			}

			/* commented as array_revere used to revert the array values and then ids for punch time are reversed */
			//$rev_exp_datas = array_reverse($exp_datas);
			foreach($exp_datas as $data) {
				//$tdata = trim($data);
				$emp_id  = $data['user_id'];//substr($tdata, 0, 5);
				$in_time = $data['time'];//substr($tdata, 6, 5);
				if($emp_id != '') {
					$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
					if(isset($result['emp_code'])){
						$employees[] = array(
							'employee_id' => $result['emp_code'],
							'card_id' => $result['card_number'],
							'in_time' => $in_time,
							'punch_date' => $next_date,
							'fdate' => date('Y-m-d H:i:s', strtotime($next_date.' '.$in_time))
						);
					}
				}
			}
			
			usort($employees, array($this, "sortByOrder"));
			
			// echo '<pre>';
			// print_r($employees);
			// exit;

			foreach ($employees as $ekey => $evalue) {
				$this->data['employees'][] = $evalue;
			}

			// echo '<pre>';
			// print_r($this->data['employees']);
			// exit;
			
			if(isset($this->data['employees']) && count($this->data['employees']) > 0){
				//$this->model_transaction_transaction->deleteorderhistory($filter_date_start);
				//$this->model_transaction_transaction->deleteorderhistory($next_date);
				foreach ($this->data['employees'] as $fkey => $employee) {
					$exist = $this->model_transaction_transaction->getorderhistory($employee);
					if($exist == 0){
						$mystring = $employee['in_time'];
						$findme   = ':';
						$pos = strpos($mystring, $findme);
						if($pos !== false){
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2). ":" . substr($employee['in_time'], 6, 2);
						} else {
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2). ":" . substr($employee['in_time'], 4, 2);
						}
						if($in_times != ':'){
							$in_time = date("h:i:s", strtotime($in_times));
						} else {
							$in_time = '';
						}
						$employee['in_time'] = $in_times;
						// echo '<pre>';
						// print_r($employee);
						// exit;
						$this->model_transaction_transaction->insert_attendance_data($employee);
					}
				}
			}
			//echo 'Done';exit;
			
			// $update1 = "UPDATE `oc_attendance` SET `status` = '0' WHERE punch_date = '".$filter_date_start."'";
			// $this->db->query($update1);
			// $update1 = "UPDATE `oc_attendance` SET `status` = '0' WHERE punch_date = '".$next_date."'";
			// $this->db->query($update1);
			
			// $update2 = "DELETE FROM `oc_transaction` WHERE `date` = '".$filter_date_start."'";
			// if($filter_unit){
			// 	$update2 .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
			// }
			// $this->db->query($update2);
			
			// $update2 = "DELETE FROM `oc_transaction` WHERE `date` = '".$next_date."'";
			// if($filter_unit){
			// 	$update2 .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
			// }
			// $this->db->query($update2);
			
			$is_exist = $this->model_report_common_report->getattendance_exist($filter_date_start);
			if($is_exist == 1){
				$data = array();
				$data['unit'] = $filter_unit;
				$results = $this->model_report_common_report->getemployees($data);
				foreach ($results as $rkey => $rvalue) {
					$rvaluess = $this->model_transaction_transaction->getrawattendance_group_date_custom($rvalue['emp_code'], $filter_date_start);
					// echo 'Emp Id : ' . $rvalue['emp_code'];
					// echo '<br />';

					//echo "<pre>"; print_r($rvaluess); exit;
					if($rvaluess) {
						//$raw_attendance_datass = $this->model_transaction_transaction->getrawattendance_group();
						//if($raw_attendance_datass) {
						//foreach ($raw_attendance_datass as $rkeyss => $rvaluess) {
						$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
						if(isset($emp_data['name']) && $emp_data['name'] != ''){
							
							$emp_name = $emp_data['name'];
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];

							$day_date = date('j', strtotime($filter_date_start));
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));

							$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvaluess['emp_id']."' ";
							//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
							$shift_schedule = $this->db->query($update3)->row;
							$schedule_raw = explode('_', $shift_schedule[$day_date]);
							if(!isset($schedule_raw[2])){
								$schedule_raw[2] = 1;
							}
							if($schedule_raw[0] == 'S'){
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								if(isset($shift_data['shift_id'])){
									$abnormal_status = 0;
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){ 
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										
										// echo '<pre>';
										// print_r($act_intimes);
										
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										
										// echo '<pre>';
										// print_r($act_outtimes);
										// exit;
										
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										if($trans_exist->row['act_intime'] == '00:00:00'){
											$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
											$new_data_exist = $this->db->query($new_data_sql);
											if($new_data_exist->num_rows > 0){	
												$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
												if(isset($act_intimes['punch_time'])) {
													$act_intime = $act_intimes['punch_time'];
													$act_in_punch_date = $act_intimes['punch_date'];
												}
											}	
										} else {
											$act_intime = $trans_exist->row['act_intime'];
											$act_in_punch_date = $trans_exist->row['date'];	
										}
									
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){
											$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
											if(isset($act_outtimes['punch_time'])) {
												$act_outtime = $act_outtimes['punch_time'];
												$act_out_punch_date = $act_outtimes['punch_date'];
											}
										} else {
											$act_outtime = $trans_exist->row['act_outtime'];
											$act_out_punch_date = $trans_exist->row['date_out'];
										}
									}

									// if($filter_date_start != $act_in_punch_date){
									// 	$abnormal_status = 1;
									// 	$act_in_punch_date = $filter_date_start;
									// }


									// echo '<pre>';
									// print_r($act_intimes);
									
									// $act_status = 0;
									// if(isset($act_intimes['punch_time']) && $act_intimes['status'] == 1){
									// 	$act_intime = $act_intimes['punch_time'];
									// 	$act_in_punch_date = $act_intimes['punch_date'];
									// 	$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_intimes['punch_date']);
									// 	$act_outtime = $act_outtimes['punch_time'];
									// 	$act_out_punch_date = $act_outtimes['punch_date'];
									// 	$act_status = 1;
									// } elseif(isset($act_intimes['punch_time']) && $act_intimes['status'] == 0){
									// 	$act_intime = $act_intimes['punch_time'];
									// 	$act_in_punch_date = $act_intimes['punch_date'];
									// 	$act_status = 0;
									// }

									// echo '<pre>';
									// print_r($act_outtimes);
									// exit;
									
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									//$act_intime = "09:15:00";
									//$act_outtime = "17:20:00";
									if($shift_data['lunch'] == 1){
										$first_half = 0;
										$second_half = 0;
										$late_time = '00:00:00';
										if($act_intime != '00:00:00'){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$shift_outtime));
											$minus_hours = 0;
											if($since_start->h > 8){
												if($since_start->h == 9){
													$minus_hours = 1;
												} elseif($since_start->h == 10){
													$minus_hours = 2;
												} elseif($since_start->h == 11){
													$minus_hours = 3;
												} elseif($since_start->h == 12){
													$minus_hours = 4;
												} elseif($since_start->h == 13){
													$minus_hours = 5;
												} elseif($since_start->h == 14){
													$minus_hours = 6;
												}
											}
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											if($since_start->invert == 1){
												$late_time = '00:00:00';
											}
											if($since_start->invert == 1 && $since_start->h >= 3){ //abnormal status condition
												$abnormal_status = 1;
											} else {
												//$shift_intime_plus_three = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
												//$start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_three);
												//$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
												// echo '<pre>';
												// print_r($since_start);
												if($since_start->h >= 1 && $since_start->i >= 1 && $since_start->invert == 0){
												//if( ($since_start->h < 3 && $since_start->invert == 1) || $since_start->invert == 0){ //if actintime and half time i.e shift time + 4 hours difference is greater than 3 make his first half present else absent
													//echo 'in if'; exit;
													//if($since_start->h >= 3 && $since_start->invert == 0){ //if shiftintime is greater than act intme by 3h condition
													$first_half = 0;
													if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
														$since_start_hrs = $since_start->h - $minus_hours;
														if($since_start_hrs >= 3 && $since_start->invert == 0){
															$second_half = 1;
														} else {
															$second_half = 0;
														}
													} else {
														$second_half = 0;
													}
												} else {
													if($act_outtime != '00:00:00'){
														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
														$since_start_hrs = $since_start->h - $minus_hours;
														if($since_start_hrs >= 6){
															$first_half = 1;
															$second_half = 1;
														} elseif($since_start_hrs >= 3){
															$first_half = 1;
															$second_half = 0;
														} else {
															$first_half = 0;
															$second_half = 0;
														}
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												}
											}
										} else {
											$first_half = 0;
											$second_half = 0;
										}

										$working_time = '00:00:00';
										$early_time = '00:00:00';
										if($abnormal_status == 0){ //if abnormal status is zero calculate further 
											$early_time = '00:00:00';
											if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h > 12){
													$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
													$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
												}
												$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
												if($since_start->invert == 0){
													$early_time = '00:00:00';
												}
											}					
											
											$working_time = '00:00:00';
											if($act_outtime != '00:00:00'){//for getting working time
												$working_time_def = '08:00:00';
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
												$minus_in = '00:00:00';
												if($since_start->invert == 0){
													$minus_in = $since_start->h.':'.$since_start->i.':'.$since_start->s;
												}
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												$minus_out = '00:00:00';
												if($since_start->invert == 1){
													$minus_out = $since_start->h.':'.$since_start->i.':'.$since_start->s;
												}
												$secs = strtotime($minus_out)-strtotime("00:00:00");
												$result = date("H:i:s",strtotime($minus_in)+$secs);
												$secs = strtotime($result)-strtotime("00:00:00");
												$working_time = date("H:i:s",strtotime($working_time_def)-$secs);
												
											}
										} else {
											$first_half = 0;
											$second_half = 0;
										}
										// echo '<br />';
										// echo $working_time;
										// echo '<br />';
										// echo $first_half;
										// echo '<br />';
										// echo $second_half;
										// echo '<br />';

										// echo 'out';exit;
									} else {
										$first_half = 0;
										$second_half = 0;
										$late_time = '00:00:00';
										
										// $act_intime = '12:05:00';
										// $act_outtime = '17:55:00';

										if($act_intime != '00:00:00'){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
											}
											$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											if($since_start->invert == 1){
												$late_time = '00:00:00';
											}
											if($since_start->invert == 1 && $since_start->h >= 3){ //abnormal status condition
												$abnormal_status = 1;
											} else {
												// $shift_intime_plus_three = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
												// $start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_three);
												// $since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
												
												// echo '<pre>';
												// print_r($since_start);
												if($since_start->h >= 1 && $since_start->i >= 1 && $since_start->invert == 0){
												//if( ($since_start->h < 3 && $since_start->invert == 1) || $since_start->invert == 0){ //if actintime and half time i.e shift time + 4 hours difference is greater than 3 make his first half present else absent
													//echo 'in if'; exit;
													//if($since_start->h >= 3 && $since_start->invert == 0){ //if shiftintime is greater than act intme by 3h condition
													$first_half = 0;
													if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
														$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
														$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
														
														// echo '<pre>';
														// print_r($since_start);
														// exit;

														if($since_start->h >= 3 && $since_start->invert == 0){
															$second_half = 1;
														} else {
															$second_half = 0;
														}
													} else {
														$second_half = 0;
													}
												} else {
													//echo 'in else'; exit;
													if($act_outtime != '00:00:00'){
														// $compare_time = Date('h:i:s', strtotime($shift_intime .' +4 hour'));
														// $start_date = new DateTime($act_in_punch_date.' '.$compare_time);
														// $since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));

														$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
														$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
														
														// echo '<pre>';
														// print_r($act_in_punch_date.' '.$shift_intime);

														// echo '<pre>';
														// print_r($act_out_punch_date.' '.$act_outtime);

														// echo '<pre>';
														// print_r($since_start);
														// exit;

														if($since_start->h >= 6){
															$first_half = 1;
															$second_half = 1;
														} elseif($since_start->h >= 3){
															$first_half = 1;
															$second_half = 0;
														} else {
															$first_half = 0;
															$second_half = 0;
														}
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												}
											}
										} else {
											$first_half = 0;
											$second_half = 0;
										}

										// echo '<pre>';
										// print_r($first_half);
										// echo '<pre>';
										// print_r($second_half);
										// exit;

										//echo 'out';exit;

										$working_time = '00:00:00';
										$early_time = '00:00:00';
										if($abnormal_status == 0){ //if abnormal status is zero calculate further 
											$early_time = '00:00:00';
											if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
												$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												if($since_start->h > 12){
													$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
													$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
												}
												$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
												if($since_start->invert == 0){
													$early_time = '00:00:00';
												}
											}					
											
											$working_time = '00:00:00';
											if($act_outtime != '00:00:00'){//for getting working time
												$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
												$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
												$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											}
										} else {
											$first_half = 0;
											$second_half = 0;
										}
									}

									$abs_stat = 0;
									if($filter_date_start != $act_in_punch_date){
										$abs_stat = 1;
										$act_in_punch_date = $filter_date_start;
									}

									if($abs_stat == 0){
										if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
											$abnormal_status = 1;
										}
									}

									if($working_time != '00:00:00'){
										$work_exp = explode(':', $working_time);
										if($work_exp[0] >= 18){
											$abnormal_status = 1;
										}
									}

									if($early_time != '00:00:00'){
										$early_exp = explode(':', $early_time);
										if($early_exp[0] >= 6){
											$abnormal_status = 1;
										}
									}

									if($late_time != '00:00:00'){
										$late_exp = explode(':', $late_time);
										if($late_exp[0] >= 6){
											$abnormal_status = 1;
										}
									}

									if($abnormal_status == 1){
										$first_half = 0;
										$second_half = 0;										
										//$early_time = '00:00:00';
										//$late_time = '00:00:00';
										//$working_time = '00:00:00';										
									}

									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									$day = date('j', strtotime($rvaluess['punch_date']));
									$month = date('n', strtotime($rvaluess['punch_date']));
									$year = date('Y', strtotime($rvaluess['punch_date']));
									//echo 'out';exit;
									if($trans_exist->num_rows == 0){
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0' ";
										} else {
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->getLastId();
									} else {
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										} else {
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
									}
									// echo $sql.';';
									// echo '<br />';
									// exit;
									
								}
								//echo 'out1';exit;
							} elseif ($schedule_raw[0] == 'W') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}
								$abnormal_status = 0;

								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
								
							} elseif ($schedule_raw[0] == 'H') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}


									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							} elseif ($schedule_raw[0] == 'HD') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								//$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								// echo '<pre>';
								// print_r($act_intime);
								// echo '<pre>';
								// print_r($act_outtime);
								// exit;

								$abnormal_status = 0;
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($shift_data['lunch'] == 1){
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$shift_outtime));
										$minus_hours = 0;
										if($since_start->h > 8){
											if($since_start->h == 9){
												$minus_hours = 1;
											} elseif($since_start->h == 10){
												$minus_hours = 2;
											} elseif($since_start->h == 11){
												$minus_hours = 3;
											} elseif($since_start->h == 12){
												$minus_hours = 4;
											} elseif($since_start->h == 13){
												$minus_hours = 5;
											} elseif($since_start->h == 14){
												$minus_hours = 6;
											}
										}
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										if($since_start->invert == 1){
											$late_time = '00:00:00';
										}
										if($since_start->invert == 1 && $since_start->h >= 3){ //abnormal status condition
											$abnormal_status = 1;
										} else {
											// $shift_intime_plus_three = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
											// $start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_three);
											// $since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											// echo '<pre>';
											// print_r($since_start);
											if($since_start->h >= 1 && $since_start->i >= 1 && $since_start->invert == 0){
											//if( ($since_start->h < 3 && $since_start->invert == 1) || $since_start->invert == 0){ //if actintime and half time i.e shift time + 4 hours difference is greater than 3 make his first half present else absent
												//echo 'in if'; exit;
												//if($since_start->h >= 3 && $since_start->invert == 0){ //if shiftintime is greater than act intme by 3h condition
												$first_half = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													$since_start_hrs = $since_start->h - $minus_hours;
													if($since_start_hrs >= 3 && $since_start->invert == 0){
														$second_half = 'HD';
													} else {
														$second_half = 0;
													}
												} else {
													$second_half = 0;
												}
											} else {
												if($act_outtime != '00:00:00'){
													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													$since_start_hrs = $since_start->h - $minus_hours;
													if($since_start_hrs >= 3){
														$first_half = 1;
														$second_half = 'HD';
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$working_time = '00:00:00';
									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
										
										$working_time = '00:00:00';
										if($act_outtime != '00:00:00'){//for getting working time
											$working_time_def = '08:00:00';
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											$minus_in = '00:00:00';
											if($since_start->invert == 0){
												$minus_in = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											}
											$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$minus_out = '00:00:00';
											if($since_start->invert == 1){
												$minus_out = $since_start->h.':'.$since_start->i.':'.$since_start->s;
											}
											$secs = strtotime($minus_out)-strtotime("00:00:00");
											$result = date("H:i:s",strtotime($minus_in)+$secs);
											$secs = strtotime($result)-strtotime("00:00:00");
											$working_time = date("H:i:s",strtotime($working_time_def)-$secs);
											
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									// echo '<br />';
									// echo $working_time;
									// echo '<br />';
									// echo $first_half;
									// echo '<br />';
									// echo $second_half;
									// echo '<br />';

									// echo 'out';exit;
								} else {
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									
									// $act_intime = '12:05:00';
									// $act_outtime = '17:55:00';

									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										if($since_start->invert == 1){
											$late_time = '00:00:00';
										}
										if($since_start->invert == 1 && $since_start->h >= 3){ //abnormal status condition
											$abnormal_status = 1;
										} else {
											// $shift_intime_plus_three = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
											// $start_date = new DateTime($act_in_punch_date.' '.$shift_intime_plus_three);
											// $since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
											
											// echo '<pre>';
											// print_r($since_start);
											if($since_start->h >= 1 && $since_start->i >= 1 && $since_start->invert == 0){
											//if( ($since_start->h < 3 && $since_start->invert == 1) || $since_start->invert == 0){ //if actintime and half time i.e shift time + 4 hours difference is greater than 3 make his first half present else absent
												//echo 'in if'; exit;
												//if($since_start->h >= 3 && $since_start->invert == 0){ //if shiftintime is greater than act intme by 3h condition
												$first_half = 0;
												if($act_outtime != '00:00:00'){ //to get second half actintime and actouttime difference should be greater then or equal to 3 hrs
													$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													
													// echo '<pre>';
													// print_r($since_start);
													// exit;

													if($since_start->h >= 3 && $since_start->invert == 0){
														$second_half = 'HD';
													} else {
														$second_half = 0;
													}
												} else {
													$second_half = 0;
												}
											} else {
												//echo 'in else'; exit;
												if($act_outtime != '00:00:00'){
													// $compare_time = Date('h:i:s', strtotime($shift_intime .' +4 hour'));
													// $start_date = new DateTime($act_in_punch_date.' '.$compare_time);
													// $since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));

													$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
													$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
													
													// echo '<pre>';
													// print_r($act_in_punch_date.' '.$shift_intime);

													// echo '<pre>';
													// print_r($act_out_punch_date.' '.$act_outtime);

													// echo '<pre>';
													// print_r($since_start);
													// exit;

													if($since_start->h >= 3){
														$first_half = 1;
														$second_half = 'HD';
													} else {
														$first_half = 0;
														$second_half = 0;
													}
												} else {
													$first_half = 0;
													$second_half = 0;
												}
											}
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									// echo '<pre>';
									// print_r($first_half);
									// echo '<pre>';
									// print_r($second_half);
									// exit;

									//echo 'out';exit;

									$working_time = '00:00:00';
									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
										
										$working_time = '00:00:00';
										if($act_outtime != '00:00:00'){//for getting working time
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}
								}

								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								if($abs_stat == 0){
									if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
										$abnormal_status = 1;
									}
								}

								if($working_time != '00:00:00'){
									$work_exp = explode(':', $working_time);
									if($work_exp[0] >= 18){
										$abnormal_status = 1;
									}
								}

								if($early_time != '00:00:00'){
									$early_exp = explode(':', $early_time);
									if($early_exp[0] >= 6){
										$abnormal_status = 1;
									}
								}

								if($late_time != '00:00:00'){
									$late_exp = explode(':', $late_time);
									if($late_exp[0] >= 6){
										$abnormal_status = 1;
									}
								}

								if($abnormal_status == 1){
									$first_half = 0;
									$second_half = 0;										
									//$early_time = '00:00:00';
									//$late_time = '00:00:00';
									//$working_time = '00:00:00';										
								}

								if($first_half == 1 && $second_half == 'HD'){
									$present_status = 1;
									$absent_status = 0;
								} else {
									$present_status = 0;
									$absent_status = 1;
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							}  elseif ($schedule_raw[0] == 'C') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";	
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							}
							if($act_in_punch_date == $act_out_punch_date) {
								if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								}
							} else {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								$this->db->query($update);
								$this->log->write($update);

								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								$this->db->query($update);
								$this->log->write($update);
							}
						}
					} else {
						$emp_data = $this->model_transaction_transaction->getempdata($rvalue['emp_code']);
						if(isset($emp_data['name']) && $emp_data['name'] != ''){
							$emp_name = $emp_data['name'];
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];

							$day_date = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));

							$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' ";
							//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
							$shift_schedule = $this->db->query($update3)->row;
							$schedule_raw = explode('_', $shift_schedule[$day_date]);
							if($schedule_raw[0] == 'S'){
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
								if(isset($shift_data['shift_id'])){
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."'";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									}
								} else {
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									}
								}
							} elseif ($schedule_raw[0] == 'W') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							} elseif ($schedule_raw[0] == 'H') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0'  ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							} elseif ($schedule_raw[0] == 'HD') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								//$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							}  elseif ($schedule_raw[0] == 'C') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							}
							// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
							// $this->db->query($update);
							// $this->log->write($update);
						}	
					}
				}
				
				// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."'";
				// $this->db->query($update);
				// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$next_date."'";
				// $this->db->query($update);
				
				$this->db->query("commit;");

				//echo 'out';exit;
				//echo 'Done';exit;
				$this->session->data['success'] = 'Transaction Generated';
				if(isset($this->request->get['home'])){
					$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
				} elseif(isset($this->request->get['transaction'])){
					$url = '&filter_date_start='.$filter_date_start;
					if($filter_unit){
						$url .= '&unit='.$filter_unit;
					}
					$this->redirect($this->url->link('transaction/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
				} else {
					$url = '&filter_date_start='.$filter_date_start;
					if($filter_unit){
						$url .= '&unit='.$filter_unit;
					}
					$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
				}
			} else {

				$this->db->query("commit;");

				$this->session->data['warning'] = 'No Data available';
				if(isset($this->request->get['home'])){
					$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
				} elseif(isset($this->request->get['transaction'])){
					$url = '&filter_date_start='.$filter_date_start;
					if($filter_unit){
						$url .= '&unit='.$filter_unit;
					}
					$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
				} else {
					$url = '&filter_date_start='.$filter_date_start;
					if($filter_unit){
						$url .= '&unit='.$filter_unit;
					}
					$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
				}
				//$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'], 'SSL'));
			}
		} else {
			$this->db->query("commit;");

			$this->session->data['warning'] = 'Day already Closed';
			if(isset($this->request->get['home'])){
				$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
			}  elseif(isset($this->request->get['transaction'])){
				$url = '&filter_date_start='.$filter_date_start;
				if($filter_unit){
					$url .= '&unit='.$filter_unit;
				}
				$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
			} else {
				$url = '&filter_date_start='.$filter_date_start;
				$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
			}
		}
	}
}
?>