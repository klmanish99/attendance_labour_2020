<?php
/*
@author: Eric
This code is for leave processing.
First we select all the unprocessed leaves for a particular location and display on screen.
When the "Leave Process" button is clicked 

*/
class ControllerTransactionLeaveprocess extends Controller { 
	public function index() {  
		$this->language->load('transaction/leaveprocess');
		$this->load->model('transaction/leaveprocess');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = '';
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}

		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}

		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		

		$this->data['exceptional'] = array();

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;


		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;

		$unprocessed = array();
		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			$data = array(
				'unit'				 => $unit,
				'department'			 => $department,
				'division'			 	 => $division,
				'region'			 	 => $region,
				'company'			 	 => $company,
				'start'              => ($page - 1) * 7000,
				'limit'              => 7000
			);
			$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate($data);
			//foreach($unprocessed as $akey => $avalue){
				//$action = array();
				//if($avalue['a_status'] == '0'){
					// $action[] = array(
					// 	'text' => 'Activate',
					// 	'href' => $this->url->link('transaction/leave/active', 'token=' . $this->session->data['token'] . '&batch_id=' . $avalue['batch_id'] . '&filter_name_id=' . $avalue['emp_id'] . '&unit=' . $unit . '&leaveprocess=1', 'SSL')
					// );
					// $action[] = array(
					// 	'text' => 'In Active',
					// 	'href' => ''
					// );	
				//} else {
					// $action[] = array(
					// 	'text' => 'Active',
					// 	'href' => ''
					// );
				//}
				// $action[] = array(
				// 	'text' => 'View',
				// 	'href' => $this->url->link('transaction/leave/getForm', 'token=' . $this->session->data['token'] . '&batch_id=' . $avalue['batch_id'] . '&filter_name_id=' . $avalue['emp_id'] . '&unit=' . $unit . '&leaveprocess=1', 'SSL')
				// );
				// $action[] = array(
				// 	'text' => 'Reject',
				// 	'href' => $this->url->link('transaction/leave/reject', 'token=' . $this->session->data['token'] . '&batch_id=' . $avalue['batch_id'] . '&unit=' . $unit . '&leaveprocess=1', 'SSL')
				// );
				//$unprocessed[$akey]['action'] = $action;
			//}
		}
		// echo "<pre>"; 
		// print_r($unprocessed);
		// exit;
		$this->data['unprocessed'] = $unprocessed;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date'] = $this->language->get('entry_date');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}

		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}

		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;
		
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = $company;

		$this->template = 'transaction/leaveprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function close_day() {
		
		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = '';
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
			//$company = 0;
		} else {
			$company = 0;
		}

		$data = array(
			'unit'				 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
		);
		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($data);
		// echo '<pre>';
		// print_r($unprocessed);
		// exit;
		foreach($unprocessed as $data) {
			if(strtotime($data['date']) < strtotime(date('Y-m-d'))){
				$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			}
			//$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			//if($is_closed == 1){
				//$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			//}
		}
		$url = '';
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}

		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}

		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		$this->redirect($this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}

	public function close_day_1() {
		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');

		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_1();
		// echo '<pre>';
		// print_r($unprocessed);
		// exit;
		foreach($unprocessed as $data) {
			$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			if($is_closed == 1){
				$this->model_transaction_dayprocess->checkLeave_1($data['date'], $data['emp_id']);
			}
		}
		echo 'out';exit;

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		$this->redirect($this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}	

	
}
?>