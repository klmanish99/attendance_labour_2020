<?php
class ControllerReportContractor extends Controller { 
	public function index() {  
		$this->language->load('report/contractor');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-d');
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = '';
		}

		if (isset($this->request->get['filter_location'])) {
			$filter_location = html_entity_decode($this->request->get['filter_location']);
		} else {
			$filter_location = '';
		}
		
		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = '';
		}

		if (isset($this->request->get['filter_designation'])) {
			$filter_designation = html_entity_decode($this->request->get['filter_designation']);
		} else {
			$filter_designation = '';
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = html_entity_decode($this->request->get['filter_category']);
		} else {
			$filter_category = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/contractor', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_division'		=> $filter_division,
			'filter_location'		=> $filter_location,
			'filter_contractor'		=> $filter_contractor,
			'filter_designation'	=> $filter_designation,
			'filter_category'		=> $filter_category,
		);

		$this->data['token'] = $this->session->data['token'];
		
		$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
		}

		$this->data['final_datas'] = array();
		if(isset($this->request->get['once'])){
			$sql = "SELECT AttendanceDate, Location, PANNO, VoterIdNo, CategoryId, Designation, COUNT(e.EmployeeId) as TotalPresent FROM [etimetracklite1].[dbo].[Employees] emp LEFT JOIN [etimetracklite1].[dbo].[ATTENDANCELOGS] e ON emp.EmployeeId = e.EmployeeId WHERE 1=1 AND InTime <> '00:00' ";
			if($filter_date_start){
				$sql .= " AND CAST(AttendanceDate as DATE) >= '".$filter_date_start."' ";
			}
			if($filter_date_end){
				$sql .= " AND CAST(AttendanceDate as DATE) <= '".$filter_date_end."' ";
			}
			if($filter_division){
				$sql .= " AND PANNO = '".$filter_division."' ";
			}
			if($filter_location){
				$sql .= " AND Location = '".$filter_location."' ";
			}
			if($filter_contractor){
				$sql .= " AND VoterIdNo = '".$filter_contractor."' ";
			}
			if($filter_category){
				$sql .= " AND CategoryId = ".$filter_category." ";
			}
			if($filter_designation){
				$sql .= " AND Designation = '".$filter_designation."' ";
			}
			$sql .= " GROUP BY AttendanceDate, PANNO, Location, VoterIdNo, CategoryId, Designation ORDER BY AttendanceDate, PANNO, Location, VoterIdNo, CategoryId, Designation";
			//echo $sql;exit;
			$params = array();
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt = sqlsrv_query($conn, $sql, $params, $options);
			$attendance_datas = array();
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				// echo '<pre>';
				// print_r($row);
				// exit;
				$contractor_name = '';
				$contractor_code = '';
				if($row['VoterIdNo'] != '' && $row['VoterIdNo'] != null){
					$sql1 = "SELECT ContractorId, ContractorsName, ContractorCode FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE ContractorsName = '".$row['VoterIdNo']."' ";
					//echo $sql1;exit;
					$params1 = array();
					$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
					while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
						$contractor_name = $row1['ContractorsName'];
						$contractor_code = $row1['ContractorCode'];
					}
				}
				$category_name = '';
				if($row['CategoryId'] != '' && $row['CategoryId'] != null){
					$sql2 = "SELECT CategoryId, CategoryName FROM [etimetracklite1].[dbo].[Categories] WHERE CategoryId = ".$row['CategoryId']." ";
					//echo $sql2;exit;
					$params2 = array();
					$options2 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt2 = sqlsrv_query($conn, $sql2, $params2, $options2);
					while($row2 = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC)) {
						$category_name = $row2['CategoryName'];
					}
				}
				$attendance_dates = $row['AttendanceDate'];
				$attendance_date = '';
				if($attendance_dates != null){	
					$attendance_date = $attendance_dates->format("Y-m-d");
				}
				$attendance_datas[] = array(
					'location' => $row['Location'],
					'division' => $row['PANNO'],
					'contractor_code' => $contractor_code,
					'contractor_name' => $contractor_name,
					'date' => $attendance_date,
					'category_name' => $category_name,
					'designation' => $row['Designation'],
					'total_count' => $row['TotalPresent'],
				);
				$this->data['final_datas'] = $attendance_datas;
			}
		}
		// echo '<pre>';
		// print_r($attendance_datas);
		// exit;
		
		$sql = "SELECT DivisionId, DivisionName FROM [etimetracklite1].[dbo].[Divisions] WHERE 1=1 ";
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$division_datas = array();
		$division_datas[] = array(
			'division_id' => '',
			'division_name' => 'All',
		);
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$division_datas[] = array(
				'division_id' => $row['DivisionName'],
				'division_name' => $row['DivisionName'],
			);
		}
		$this->data['division_datas'] = $division_datas;

		$sql = "SELECT Location FROM [etimetracklite1].[dbo].[Employees] WHERE 1=1 AND Location IS NOT NULL ";
		if($filter_division){
			$sql .= " AND PANNO = '".$filter_division."' ";
		}
		$sql .= " GROUP BY Location ";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$location_datas = array();
		$location_datas[] = array(
			'location_id' => '',
			'location_name' => 'All',
		);
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			 $location_datas[] = array(
				'location_id' => $row['Location'],
				'location_name' => $row['Location'],
			);
		}
		$this->data['location_datas'] = $location_datas;
	
		// $sql = "SELECT ContractorId, ContractorsName, ContractorCode, LocationName FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE 1=1 ";
		// if($filter_location){
		// 	$sql .= " AND (LocationName LIKE '%".$filter_location."%' OR LocationName IS NULL OR LocationName = '') ";
		// }
		// $sql .= " ORDER BY LocationName DESC ";
		$sql = "SELECT ContractorId FROM [etimetracklite1].[dbo].[Employees] WHERE 1=1 AND ContractorId IS NOT NULL ";
		if($filter_location){
			$sql .= " AND Location = '".$filter_location."' ";
		}
		$sql .= " GROUP BY ContractorId ";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$contractor_datas = array();
		$contractor_datas[] = array(
			'contractor_id' => '',
			'contractor_name' => 'All',
			'contractor_code' => '',
		);
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$sql1 = "SELECT ContractorId, ContractorsName, ContractorCode, LocationName FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE ContractorId = ".$row['ContractorId']." ";
			//echo $sql1;exit;
			//$this->log->write(print_r($sql1, true));
			$params1 = array();
			$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
			while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
				$contractor_datas[] = array(
					'contractor_id' => $row1['ContractorId'],
					'contractor_name' => $row1['ContractorsName'],
					'contractor_code' => $row1['ContractorCode'],
				);
			}
		}
		$this->data['contractor_datas'] = $contractor_datas;
		
		$sql = "SELECT CategoryId, CategoryName FROM [etimetracklite1].[dbo].[Categories] WHERE 1=1 ";
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$category_datas = array();
		$category_datas[] = array(
			'category_id' => '',
			'category_name' => 'All',
		);
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$category_datas[] = array(
		    	'category_id' => $row['CategoryId'],
		    	'category_name' => $row['CategoryName'],
		    );
		}
		// $category_datas = array(
		// 	'0' => 'ALL',
		// 	'1' => 'SUPPLY',
		// );
		$this->data['category_datas'] = $category_datas;
		
		$sql = "SELECT DesignationId, DesignationsName FROM [etimetracklite1].[dbo].[Designations] WHERE 1=1 ";
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$designation_datas = array();
		$designation_datas[] = array(
			'designation_id' => '',
			'designation_name' => 'All',
		);
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$designation_datas[] = array(
		    	'designation_id' => $row['DesignationsName'],
		    	'designation_name' => $row['DesignationsName'],
		    );
		}
		// $designation_datas = array(
		// 	'0' => 'ALL',
		// 	'1' => 'HELPER',
		// 	'2' => 'STONE BREAKER',
		// 	'3' => 'MASON',
		// );
		$this->data['designation_datas'] = $designation_datas;
	
		$fields_data = array(
			'1' => 'Division',
			'2' => 'Location',
			'3' => 'Contractor Code',
			'4' => 'Contractor Name',
			'5' => 'Date',
			'6' => 'Labour Category',
			'7' => 'Designation',
			'8' => 'Total Count',
		);
		$this->data['fields_data'] = $fields_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_division'])) {
			$url .= '&filter_division=' . $this->request->get['filter_division'];
		}
		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}
		if (isset($this->request->get['filter_contractor'])) {
			$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		if (isset($this->request->get['filter_fields'])) {
			$url .= '&filter_fields=' . $this->request->get['filter_fields'];
		}
		$url .= '&once=1';
		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$pagination = new Pagination();
		$pagination->total = 0;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('report/contractor', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_division'] = $filter_division;
		$this->data['filter_location'] = $filter_location;
		$this->data['filter_contractor'] = $filter_contractor;
		$this->data['filter_designation'] = $filter_designation;
		$this->data['filter_category'] = $filter_category;
		$this->data['filter_fields'] = $filter_fields;

		$this->template = 'report/contractor.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		date_default_timezone_set("Asia/Kolkata");
		$this->language->load('report/contractor');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y-m-d');
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_fields'])) {
			$filter_fields = $this->request->get['filter_fields'];
			$filter_fields = explode(',', $filter_fields);
		} else {
			$filter_fields = array();
		}

		if (isset($this->request->get['filter_division'])) {
			$filter_division = html_entity_decode($this->request->get['filter_division']);
		} else {
			$filter_division = '';
		}

		if (isset($this->request->get['filter_location'])) {
			$filter_location = html_entity_decode($this->request->get['filter_location']);
		} else {
			$filter_location = '';
		}
		
		if (isset($this->request->get['filter_contractor'])) {
			$filter_contractor = html_entity_decode($this->request->get['filter_contractor']);
		} else {
			$filter_contractor = '';
		}

		if (isset($this->request->get['filter_designation'])) {
			$filter_designation = html_entity_decode($this->request->get['filter_designation']);
		} else {
			$filter_designation = '';
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = html_entity_decode($this->request->get['filter_category']);
		} else {
			$filter_category = '';
		}
		

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_division'		=> $filter_division,
			'filter_location'		=> $filter_location,
			'filter_contractor'		=> $filter_contractor,
			'filter_designation'	=> $filter_designation,
			'filter_category'		=> $filter_category,
		);
		
		$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
		}
		$final_datas = array();
		$sql = "SELECT AttendanceDate, Location, PANNO, VoterIdNo, CategoryId, Designation, COUNT(e.EmployeeId) as TotalPresent FROM [etimetracklite1].[dbo].[Employees] emp LEFT JOIN [etimetracklite1].[dbo].[ATTENDANCELOGS] e ON emp.EmployeeId = e.EmployeeId WHERE 1=1 AND InTime <> '00:00' ";
		if($filter_date_start){
			$sql .= " AND CAST(AttendanceDate as DATE) >= '".$filter_date_start."' ";
		}
		if($filter_date_end){
			$sql .= " AND CAST(AttendanceDate as DATE) <= '".$filter_date_end."' ";
		}
		if($filter_division){
			$sql .= " AND PANNO = '".$filter_division."' ";
		}
		if($filter_location){
			$sql .= " AND Location = '".$filter_location."' ";
		}
		if($filter_contractor){
			$sql .= " AND VoterIdNo = '".$filter_contractor."' ";
		}
		if($filter_category){
			$sql .= " AND CategoryId = ".$filter_category." ";
		}
		if($filter_designation){
			$sql .= " AND Designation = '".$filter_designation."' ";
		}
		$sql .= " GROUP BY AttendanceDate, PANNO, Location, VoterIdNo, CategoryId, Designation ORDER BY AttendanceDate, PANNO, Location, VoterIdNo, CategoryId, Designation";
		//echo $sql;exit;
		$params = array();
		$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
		$stmt = sqlsrv_query($conn, $sql, $params, $options);
		$attendance_datas = array();
		while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			// echo '<pre>';
			// print_r($row);
			// exit;
			$contractor_name = '';
			$contractor_code = '';
			if($row['VoterIdNo'] != '' && $row['VoterIdNo'] != null){
				$sql1 = "SELECT ContractorId, ContractorsName, ContractorCode FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE ContractorsName = '".$row['VoterIdNo']."' ";
				//echo $sql1;exit;
				$params1 = array();
				$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
				while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
					$contractor_name = $row1['ContractorsName'];
					$contractor_code = $row1['ContractorCode'];
				}
			}
			$category_name = '';
			if($row['CategoryId'] != '' && $row['CategoryId'] != null){
				$sql2 = "SELECT CategoryId, CategoryName FROM [etimetracklite1].[dbo].[Categories] WHERE CategoryId = ".$row['CategoryId']." ";
				//echo $sql2;exit;
				$params2 = array();
				$options2 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt2 = sqlsrv_query($conn, $sql2, $params2, $options2);
				while($row2 = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC)) {
					$category_name = $row2['CategoryName'];
				}
			}
			$attendance_dates = $row['AttendanceDate'];
			$attendance_date = '';
			if($attendance_dates != null){	
				$attendance_date = $attendance_dates->format("Y-m-d");
			}
			$attendance_datas[] = array(
				'location' => $row['Location'],
				'division' => $row['PANNO'],
				'contractor_code' => $contractor_code,
				'contractor_name' => $contractor_name,
				'date' => $attendance_date,
				'category_name' => $category_name,
				'designation' => $row['Designation'],
				'total_count' => $row['TotalPresent'],
			);
			$final_datas = $attendance_datas;
		}
		
		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		//$final_datas = array_chunk($final_datas, 3);
		if($final_datas){
			if ($filter_division != ''){
				$filter_division = html_entity_decode($filter_division);
			} else {
				$filter_division = 'All';
			}
			if ($filter_location != ''){
				$filter_location = html_entity_decode($filter_location);
			} else {
				$filter_location = 'All';
			}
			if ($filter_contractor != ''){
				$sql = "SELECT ContractorsName FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE ContractorsName = '".$filter_contractor."' ";
				$params = array();
				$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET);
				$stmt = sqlsrv_query($conn, $sql, $params, $options);
				$contractor_name = '';
				while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
					$contractor_name = $row1['ContractorsName'];
				}
				$filter_contractor = html_entity_decode($contractor_name);
			} else {
				$filter_contractor = 'All';
			}
			if ($filter_category != ''){
				$sql = "SELECT CategoryName FROM [etimetracklite1].[dbo].[Categories] WHERE CategoryId = ".$filter_category." ";
				$params = array();
				$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET);
				$stmt = sqlsrv_query($conn, $sql, $params, $options);
				$category_name = '';
				while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
					$category_name = $row['CategoryName'];
				}
				$filter_category = html_entity_decode($category_name);
			} else {
				$filter_category = 'All';
			}
			if ($filter_designation != ''){
				$filter_designation = html_entity_decode($filter_designation);
			} else {
				$filter_designation = 'All';
			}
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['date_start'] = date('d-F-Y', strtotime($filter_date_start));
			$template->data['date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_location'] = $filter_location;
			$template->data['filter_contractor'] = $filter_contractor;
			$template->data['filter_category'] = $filter_category;
			$template->data['filter_designation'] = $filter_designation;
			$template->data['filter_fields'] = $filter_fields;
			$template->data['title'] = 'Contractor Attendance Data';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/contractor_html.tpl');
			//echo $html;exit;
			$filename = "Contractor_Attendance_Report";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename.".html");
			// echo $html;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$url = '';
			if (isset($this->request->get['filter_date_start'])) {
				$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
			}
			if (isset($this->request->get['filter_date_end'])) {
				$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
			}
			if (isset($this->request->get['filter_division'])) {
				$url .= '&filter_division=' . $this->request->get['filter_division'];
			}
			if (isset($this->request->get['filter_location'])) {
				$url .= '&filter_location=' . $this->request->get['filter_location'];
			}
			if (isset($this->request->get['filter_contractor'])) {
				$url .= '&filter_contractor=' . $this->request->get['filter_contractor'];
			}
			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}
			if (isset($this->request->get['filter_designation'])) {
				$url .= '&filter_designation=' . $this->request->get['filter_designation'];
			}
			if (isset($this->request->get['filter_category'])) {
				$url .= '&filter_category=' . $this->request->get['filter_category'];
			}
			if (isset($this->request->get['filter_fields'])) {
				$url .= '&filter_fields=' . $this->request->get['filter_fields'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/contractor', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function getlocation() {
		$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
		}
		$json = array();
		$location_datas = array();
		$location_datas[] = array(
			'location_id' => '',
			'location_name' => 'All',
		);
		$contractor_datas = array();
		if (isset($this->request->get['filter_division_id'])) {
			$filter_division = $this->request->get['filter_division_id'];
			$sql = "SELECT Location FROM [etimetracklite1].[dbo].[Employees] WHERE 1=1 AND Location IS NOT NULL";
			if($filter_division){
				$sql .= " AND PANNO = '".$filter_division."' ";
			}
			$sql .= " GROUP BY Location ";
			//echo $sql;exit;
			$params = array();
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt = sqlsrv_query($conn, $sql, $params, $options);
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				 $location_datas[] = array(
					'location_id' => $row['Location'],
					'location_name' => $row['Location'],
				);
			}
			// echo '<pre>';
			// print_r($location_datas);
			// exit;
			/*
			if($location_datas){
				foreach ($location_datas as $lkey => $lvalue) {
					$filter_location = $lvalue['location_name'];
					$sql1 = "SELECT ContractorId, ContractorsName, ContractorCode, LocationName FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE 1=1 ";
					if($filter_location){
						$sql1 .= " AND (LocationName LIKE '%".$filter_location."%' OR LocationName IS NULL OR LocationName = '') ";
					}
					$sql1 .= " ORDER BY LocationName DESC ";
					//echo $sql1;exit;
					$params1 = array();
					$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
					$contractor_datas = array();
					while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
						$contractor_datas[$row1['ContractorId']] = array(
							'contractor_id' => $row1['ContractorId'],
							'contractor_name' => $row1['ContractorsName'],
							'contractor_code' => $row1['ContractorCode'],
						);
					}	
				}
			}
			*/
		}
		// echo '<pre>';
		// print_r($location_datas);
		// echo '<pre>';
		// print_r($contractor_datas);
		// exit;
		$json['location_datas'] = $location_datas;
		$json['contractor_datas'] = $contractor_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function getcontractors() {
		$serverName = "JMCFRM\ATTENDANCE";
		$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"avi", "PWD"=>"avi2123");
		//$connectionInfo = array("Database"=>"etimetracklite1", "UID"=>"staff", "PWD"=>"staff");
		//$connectionInfo = array("Database"=>"SmartOffice");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		if($conn) {
			//echo "Connection established.<br />";exit;
		} else {
			// echo '<pre>';
			// print_r(sqlsrv_errors());
			// exit;
		}
		$json = array();
		$contractor_datas = array();
		$contractor_datas[] = array(
			'contractor_id' => '',
			'contractor_name' => 'All',
			'contractor_code' => '',
		);
		if (isset($this->request->get['filter_location_id'])) {
			$filter_location = $this->request->get['filter_location_id'];
			$sql = "SELECT ContractorId FROM [etimetracklite1].[dbo].[Employees] WHERE 1=1 AND ContractorId IS NOT NULL ";
			if($filter_location){
				$sql .= " AND Location = '".$filter_location."' ";
			}
			$sql .= " GROUP BY ContractorId ";
			$params = array();
			$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
			$stmt = sqlsrv_query($conn, $sql, $params, $options);
			while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				$sql1 = "SELECT ContractorId, ContractorsName, ContractorCode, LocationName FROM [etimetracklite1].[dbo].[CONTRACTOR] WHERE ContractorId = ".$row['ContractorId']." ";
				//$this->log->write(print_r($sql1, true));
				$params1 = array();
				$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
				while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
					$contractor_datas[$row1['ContractorId']] = array(
						'contractor_id' => $row1['ContractorId'],
						'contractor_name' => html_entity_decode($row1['ContractorsName'], ENT_QUOTES, 'UTF-8'),
						'contractor_code' => html_entity_decode($row1['ContractorCode'], ENT_QUOTES, 'UTF-8'),
					);
				}	
			}
		}
		//$this->log->write(print_r($contractor_datas, true));
		// echo '<pre>';
		// print_r($contractor_datas);
		// exit;
		$json['contractor_datas'] = $contractor_datas;
		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_name'])){
				$filter_department = $this->session->data['dept_name'];
			} else {
				$filter_department = null;
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>