<?php
class ControllerReportLeavecredit extends Controller { 
	public function index() {  

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		$this->language->load('report/leave_credit');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			if(date('n') <= 3){
				$filter_year = date('Y');
			} else {
				$filter_year = date('Y') - 1;
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/leave_credit', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
			'day_close'              => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$years = array(
			'2017' => '2017',
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',
		);
		$this->data['years'] = $years;

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));
		$year = $data['filter_year'];
		if($year == 2016){
			$from_month = '02';
			$from_year = $year + 1;
		} else {
			$from_month = '04';
			$from_year = $year;
		}
		$to_month = '03';
		$to_year = $year + 1;
		$filter_date_start = sprintf("%04d-%02d-%02d", $from_year, $from_month, '01');
		$tdays = cal_days_in_month(CAL_GREGORIAN, $to_month, $to_year);
		$filter_date_end = sprintf("%04d-%02d-%02d", $to_year, $to_month, $tdays);
		//$filter_date_end = '2018-02-28';
		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$month = array();
        $months = $this->GetMonth($filter_date_start, $filter_date_end);
        foreach ($months as $mkey => $mvalue) {
        	$dates = explode('-', $mvalue);
        	$month_day = ltrim($dates[1], '0');
        	$month[$month_day]['month'] = $month_day;
        	$month[$month_day]['year'] = date('Y', strtotime($mvalue));
        	$month[$month_day]['month_name'] = date('M Y', strtotime($mvalue));
        	$month[$month_day]['date'] = $mvalue;
        }
        // echo '<pre>';
        // print_r($month);
        // exit;
		// $day = array();
		// $days = $this->GetDays($filter_date_start, $filter_date_end);
		// foreach ($days as $dkey => $dvalue) {
		// 	$dates = explode('-', $dvalue);
		// 	$day[$dkey]['day'] = $dates[2];
		// 	$day[$dkey]['date'] = $dvalue;
		// }
		// $this->data['days'] = $day;
        $from_year = $filter_year;
		$final_datas = array();
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
				$pl_open = 0;
				if(isset($leave_datas['leave_id'])){
					$acc_value = $this->model_transaction_transaction->getacc_value_opening($leave_datas['leave_id'], 'PL');
					$pl_open = $acc_value;
				}
				$final_datas[$rvalue['emp_code']]['basic_data']['pl_open'] = $pl_open;
				$closing_pl = 0;
				$cnt = 0;
				foreach($month as $mkey => $mvalue){
					$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
					$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

					$leave_transaction_datas = $this->db->query("SELECT * FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$mvalue['month']."'  AND `year` = '".$mvalue['year']."' ")->rows;
					if($leave_transaction_datas){
						foreach($leave_transaction_datas as $tkey => $tvalue){
							if($cnt == 0){
								$balance_pl = ($pl_open + $closing_pl + $tvalue['leave_value']) - $tvalue['leave_taken'];
							} else {
								$balance_pl = ($closing_pl + $tvalue['leave_value']) - $tvalue['leave_taken']; 
							}
							$performance_data['action'][] = array(
								'month_name' => $mvalue['month_name'],
								'credit_pl' => $tvalue['leave_value'],
								'taken_pl' => $tvalue['leave_taken'],
								'balance_pl' => $balance_pl,
							);
							$closing_pl = $balance_pl;
						}
					} else {
						if($cnt == 0){
							$balance_pl = ($pl_open + $closing_pl);
						} else {
							$balance_pl = ($closing_pl); 
						}
						$performance_data['action'][] = array(
							'month_name' => $mvalue['month_name'],
							'credit_pl' => 0,
							'taken_pl' => 0,
							'balance_pl' => $balance_pl,
						);
						$closing_pl = $balance_pl;
					}
					$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
					$cnt ++;
				}
			}
		}
		$this->data['final_datas'] = $final_datas;
		// echo '<pre>';
		// print_r($this->data['final_datas']);
		// exit;
		$fields_data = array(
			'1' => 'Emp Id',
			'2' => 'Punch Id',
			'3' => 'Employee Name',
			'4' => 'Division',
			'5' => 'Region',
			'6' => 'Site',
			'7' => 'Department',
		);
		$this->data['fields_data'] = $fields_data;

		$this->load->model('catalog/unit');
		$data = array(
			'filter_division_id' => $division,
		);
		$site_datas = $this->model_catalog_unit->getUnits($data);
		$site_data = array();
		$site_data['0'] = 'All';
		$site_string = $this->user->getsite();
		$site_array = array();
		if($site_string != ''){
			$site_array = explode(',', $site_string);
		}
		foreach ($site_datas as $dkey => $dvalue) {
			if(!empty($site_array)){
				if(in_array($dvalue['unit_id'], $site_array)){
					$site_data[$dvalue['unit_id']] = $dvalue['unit'];			
				}
			} else {
				$site_data[$dvalue['unit_id']] = $dvalue['unit'];	
			}
		}
		$this->data['unit_data'] = $site_data;

		$this->load->model('catalog/department');
		$department_datas = $this->model_catalog_department->getDepartments();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['d_name'];
		}
		$this->data['department_data'] = $department_data;


		$this->load->model('catalog/region');
		$regions_datas = $this->model_catalog_region->getRegions();
		$region_data = array();
		$region_data['0'] = 'All';
		$region_string = $this->user->getregion();
		$region_array = array();
		if($region_string != ''){
			$region_array = explode(',', $region_string);
		}
		foreach ($regions_datas as $dkey => $dvalue) {
			if(!empty($region_array)){
				if(in_array($dvalue['region_id'], $region_array)){
					$region_data[$dvalue['region_id']] = $dvalue['region'];
				}
			} else {
				$region_data[$dvalue['region_id']] = $dvalue['region'];
			}
		}
		$this->data['region_data'] = $region_data;

		$this->load->model('catalog/company');
		$company_datas = $this->model_catalog_company->getCompanys();
		$company_data = array();
		//$company_data['0'] = 'All';
		$company_string = $this->user->getCompanyId();
		$company_array = array();
		if($company_string != ''){
			$company_array = explode(',', $company_string);
		}
		foreach ($company_datas as $dkey => $dvalue) {
			if(!empty($company_array)){
				if(in_array($dvalue['company_id'], $company_array)){
					$company_data[$dvalue['company_id']] = $dvalue['company'];	
				}
			} else {
				$company_data[$dvalue['company_id']] = $dvalue['company'];
			}
		}
		$this->data['company_data'] = $company_data;

		$this->load->model('catalog/division');
		$data = array(
			'filter_region_id' => $region,
		);
		$division_datas = $this->model_catalog_division->getDivisions($data);
		$division_data = array();
		$division_data['0'] = 'All';
		$division_string = $this->user->getdivision();
		$division_array = array();
		if($division_string != ''){
			$division_array = explode(',', $division_string);
		}
		foreach ($division_datas as $dkey => $dvalue) {
			if(!empty($division_array)){
				if(in_array($dvalue['division_id'], $division_array)){
					$division_data[$dvalue['division_id']] = $dvalue['division'];
				}
			} else {
				$division_data[$dvalue['division_id']] = $dvalue['division'];
			}
		}
		$this->data['division_data'] = $division_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		// echo '<pre>';
		// print_r($filter_fields);
		// exit;

		$this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['division'] = $division;
		$this->data['region'] = $region;
		$this->data['company'] = explode(',', $company);

		$this->template = 'report/leave_credit.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/leave_credit');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			if(date('n') <= 3){
				$filter_year = date('Y');
			} else {
				$filter_year = date('Y') - 1;
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = html_entity_decode($this->request->get['unit']);
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['division'])) {
			$division = html_entity_decode($this->request->get['division']);
		} else {
			$division = 0;
		}

		if (isset($this->request->get['region'])) {
			$region = html_entity_decode($this->request->get['region']);
		} else {
			$region = 0;
		}

		if (isset($this->request->get['company'])) {
			$company = html_entity_decode($this->request->get['company']);
		} else {
			$company = 0;
		}

		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'division'			 	 => $division,
			'region'			 	 => $region,
			'company'			 	 => $company,
		);

		$years = array(
			'2017' => '2017',
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',
		);
		$this->data['years'] = $years;

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));
		$year = $data['filter_year'];
		if($year == 2016){
			$from_month = '02';
			$from_year = $year + 1;
		} else {
			$from_month = '04';
			$from_year = $year;
		}
		$to_month = '03';
		$to_year = $year + 1;
		$filter_date_start = sprintf("%04d-%02d-%02d", $from_year, $from_month, '01');
		$tdays = cal_days_in_month(CAL_GREGORIAN, $to_month, $to_year);
		$filter_date_end = sprintf("%04d-%02d-%02d", $to_year, $to_month, $tdays);
		//$filter_date_end = '2018-02-28';
		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$month = array();
        $months = $this->GetMonth($filter_date_start, $filter_date_end);
        foreach ($months as $mkey => $mvalue) {
        	$dates = explode('-', $mvalue);
        	$month_day = ltrim($dates[1], '0');
        	$month[$month_day]['month'] = $month_day;
        	$month[$month_day]['year'] = date('Y', strtotime($mvalue));
        	$month[$month_day]['month_name'] = date('M Y', strtotime($mvalue));
        	$month[$month_day]['date'] = $mvalue;
        }
        // echo '<pre>';
        // print_r($month);
        // exit;
		// $day = array();
		// $days = $this->GetDays($filter_date_start, $filter_date_end);
		// foreach ($days as $dkey => $dvalue) {
		// 	$dates = explode('-', $dvalue);
		// 	$day[$dkey]['day'] = $dates[2];
		// 	$day[$dkey]['date'] = $dvalue;
		// }
		// $this->data['days'] = $day;
        $from_year = $filter_year;
		$final_datas = array();
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
			$pl_open = 0;
			if(isset($leave_datas['leave_id'])){
				$acc_value = $this->model_transaction_transaction->getacc_value_opening($leave_datas['leave_id'], 'PL');
				$pl_open = $acc_value;
			}
			$final_datas[$rvalue['emp_code']]['basic_data']['pl_open'] = $pl_open;
			$closing_pl = 0;
			$cnt = 0;
			foreach($month as $mkey => $mvalue){
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

				$leave_transaction_datas = $this->db->query("SELECT * FROM `oc_leave_credit_transaction` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$mvalue['month']."'  AND `year` = '".$mvalue['year']."' ")->rows;
				if($leave_transaction_datas){
					foreach($leave_transaction_datas as $tkey => $tvalue){
						if($cnt == 0){
							$balance_pl = ($pl_open + $closing_pl + $tvalue['leave_value']) - $tvalue['leave_taken'];
						} else {
							$balance_pl = ($closing_pl + $tvalue['leave_value']) - $tvalue['leave_taken']; 
						}
						$performance_data['action'][] = array(
							'month_name' => $mvalue['month_name'],
							'credit_pl' => $tvalue['leave_value'],
							'taken_pl' => $tvalue['leave_taken'],
							'balance_pl' => $balance_pl,
						);
						$closing_pl = $balance_pl;
					}
				} else {
					if($cnt == 0){
						$balance_pl = ($pl_open + $closing_pl);
					} else {
						$balance_pl = ($closing_pl); 
					}
					$performance_data['action'][] = array(
						'month_name' => $mvalue['month_name'],
						'credit_pl' => 0,
						'taken_pl' => 0,
						'balance_pl' => $balance_pl,
					);
					$closing_pl = $balance_pl;
				}
				$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
				$cnt ++;
			}
		}		

		$url = '';

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['division'])) {
			$url .= '&division=' . $this->request->get['division'];
		}
		if (isset($this->request->get['region'])) {
			$url .= '&region=' . $this->request->get['region'];
		}
		if (isset($this->request->get['company'])) {
			$url .= '&company=' . $this->request->get['company'];
		}
		$url .= '&once=1';
		//$final_datas = array_chunk($final_datas, 3);
		if($final_datas){
			if (isset($this->request->get['unit'])) {
				$unit_names = $this->db->query("SELECT `unit` FROM `oc_unit` WHERE `unit_id` = '".$this->request->get['unit']."' ");
				if($unit_names->num_rows > 0){
					$unit_name = $unit_names->row['unit'];
				} else {
					$unit_name = '';
				}
				$filter_unit = html_entity_decode($unit_name);
			} else {
				$filter_unit = 'All';
			}
			if (isset($this->request->get['department'])) {
				$department_names = $this->db->query("SELECT `d_name` FROM `oc_department` WHERE `department_id` = '".$this->request->get['department']."' ");
				if($department_names->num_rows > 0){
					$department_name = $department_names->row['d_name'];
				} else {
					$department_name = '';
				}
				$filter_department = html_entity_decode($department_name);
			} else {
				$filter_department = 'All';
			}
			if (isset($this->request->get['division'])) {
				$division_names = $this->db->query("SELECT `division` FROM `oc_division` WHERE `division_id` = '".$this->request->get['division']."' ");
				if($division_names->num_rows > 0){
					$division_name = $division_names->row['division'];
				} else {
					$division_name = '';
				}
				$filter_division = html_entity_decode($division_name);
			} else {
				$filter_division = 'All';
			}

			if (isset($this->request->get['region'])) {
				$region_names = $this->db->query("SELECT `region` FROM `oc_region` WHERE `region_id` = '".$this->request->get['region']."' ");
				if($region_names->num_rows > 0){
					$region_name = $region_names->row['region'];
				} else {
					$region_name = '';
				}
				$filter_region = html_entity_decode($region_name);
			} else {
				$filter_region = 'All';
			}

			if (isset($this->request->get['company'])) {
				$company_names = $this->db->query("SELECT `company` FROM `oc_company` WHERE `company_id` = '".$this->request->get['company']."' ");
				if($company_names->num_rows > 0){
					$company_name = $company_names->row['company'];
				} else {
					$company_name = '';
				}
				$filter_company = html_entity_decode($company_name);
			} else {
				$filter_company = 'All';
			}

			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['month_of'] = date('F-Y', strtotime($filter_date_end));
			$template->data['date_start'] = date('d-F-Y', strtotime($filter_date_start));
			$template->data['date_end'] = date('d-F-Y', strtotime($filter_date_end));
			$template->data['filter_division'] = $filter_division;
			$template->data['filter_region'] = $filter_region;
			$template->data['filter_company'] = $filter_company;
			$template->data['filter_unit'] = $filter_unit;
			$template->data['filter_department'] = $filter_department;
			$template->data['title'] = 'Leave Credit Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/leave_credit_html.tpl');
			//echo $html;exit;
			$filename = "Leave_Credit_Report";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename.".html");
			// echo $html;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/leave_credit', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_name'])){
				$filter_department = $this->session->data['dept_name'];
			} else {
				$filter_department = null;
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	function GetMonth($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
			// Add a day to the current date  
			$sCurrentDate = date("Y-m-d", strtotime("+1 month", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
			if($sCurrentDate < $sEndDate){
				$aDays[] = $sCurrentDate;  
			}
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}
}
?>