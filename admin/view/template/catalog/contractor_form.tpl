<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
	$part = explode('/', $this->request->get['route']);

	if (isset($part[0])) {
		$route .= $part[0];
	}

	if (isset($part[1])) {
		$route .= '/' . $part[1];
	}
}
?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
			<div class="buttons">
				<?php if($this->user->hasPermission('modify', $route) || ($this->user->hasPermission('add', $route) && !isset($this->request->get['shift_id'])) ){ ?>
					<a onclick="$('#form').submit();" class="button"><?php echo 'Save'; ?></a>
				<?php } ?>
				<a href="<?php echo $cancel; ?>" class="button"><?php echo 'Cancel'; ?></a>
			</div>
		</div>
		<div class="content">
			<div id="tabs" class="htabs"><a href="#tab-general"><?php echo 'General'; ?></a></div>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
				<div id="tab-general">
					<table class="form">
						<tr>
							<td><span class="required">*</span> <?php echo 'Name'; ?></td>
							<td><input type="text" oninput="this.value = this.value.toUpperCase()" name="contractor_name" value="<?php echo $contractor_name; ?>" size="100" />
								<?php if ($error_contractor_name) { ?>
								<span class="error"><?php echo $error_contractor_name; ?></span>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td><span class="required">*</span> <?php echo 'Code'; ?></td>
							<td><input type="text" oninput="this.value = this.value.toUpperCase()" name="contractor_code" value="<?php echo $contractor_code; ?>" size="100" />
								<?php if ($error_contractor_code) { ?>
								<span class="error"><?php echo $error_contractor_code; ?></span>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="text-align: right;">
								<a onclick="$('.checkboxclass').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$('.checkboxclass').attr('checked', false);"><?php echo 'UnSelect All'; ?></a>
							</td>
						</tr>
						<tr>
							<td><?php echo "Location"; ?></td>
							<td>
								<div class="scrollbox">
									<?php $class = 'even'; ?>
									<?php foreach ($unit_data as $dkey => $dvalue) { ?>
									<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
									 <?php //echo '<pre>';print_r($location_array); ?>
									 <div class="<?php echo $class; ?>">
										<?php  if (in_array($dkey,$location)) {  ?>
										<input class="checkboxclass" type="checkbox" name="location[]" value="<?php echo $dkey; ?>" checked="checked" />
										<?php echo $dvalue; ?>
										<?php } else { ?>
										<input class="checkboxclass" type="checkbox" name="location[]" value="<?php echo $dkey; ?>" />
										<?php echo $dvalue; ?>
										<?php } ?>
									</div>
									<?php } ?>
								</div>
							 <!-- <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php //echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php //echo 'UnSelect All'; ?></a>
							-->
							</td>            
						</tr>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 

$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<?php echo $footer; ?>