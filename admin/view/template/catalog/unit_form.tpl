<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('modify', $route) || ($this->user->hasPermission('add', $route) && !isset($this->request->get['unit_id'])) ){ ?>
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
        <a href="#tab-general"><?php echo $tab_general; ?></a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Name'; ?></td>
              <td>
                <input  oninput="this.value = this.value.toUpperCase()" tabindex="1" type="text" name="unit" value="<?php echo $unit; ?>" />
                <input tabindex="1" type="hidden" name="unit_old" value="<?php echo $unit; ?>" />
                <?php if ($error_unit) { ?>
                  <span class="error"><?php echo $error_unit; ?></span>
                <?php } ?>
              </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo 'Code'; ?></td>
              <td>
                <input  oninput="this.value = this.value.toUpperCase()" tabindex="2" type="text" name="unit_code" value="<?php echo $unit_code; ?>" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Division'; ?></td>
              <td>
                <select name="division_id" id="division_id">
                  <?php foreach($division_data as $key => $ud) { ?>
                    <?php if($key == $division_id) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_division) { ?>
                  <span class="error"><?php echo $error_division; ?></span>
                <?php } ?>
                <input type="hidden" name="division_name" id="division_name" value="<?php echo $division_name; ?>" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'State'; ?></td>
              <td>
                <select name="state_id" id="state_id">
                  <?php foreach($state_data as $key => $ud) { ?>
                    <?php if($key == $state_id) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php if ($error_state) { ?>
                  <span class="error"><?php echo $error_state; ?></span>
                <?php } ?>
                <input type="hidden" name="state_name" id="state_name" value="<?php echo $state_name; ?>" />
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#division_id').on('change', function() {
    division_name = $('#division_id option:selected').text();
    $('#division_name').val(division_name);
  });
  $('#state_id').on('change', function() {
    state_name = $('#state_id option:selected').text();
    $('#state_name').val(state_name);
  });
</script>
<?php echo $footer; ?>