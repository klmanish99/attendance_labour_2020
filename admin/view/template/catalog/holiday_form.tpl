<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
  $part = explode('/', $this->request->get['route']);

  if (isset($part[0])) {
    $route .= $part[0];
  }

  if (isset($part[1])) {
    $route .= '/' . $part[1];
  }
}
?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($this->user->hasPermission('modify', $route) || ($this->user->hasPermission('add', $route) && !isset($this->request->get['holiday_id'])) ){ ?>
          <a onclick="$('#form').submit();" class="button"><?php echo 'Save'; ?></a>
          <a href="<?php echo $process_holiday; ?>" class="button"><?php echo 'Process Holiday'; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo 'Cancel'; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo 'General'; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Name'; ?></td>
              <td><input type="text" name="name" value="<?php echo $name; ?>" size="100" />
                <?php if ($error_name) { ?>
                <span class="error"><?php echo $error_name; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Date'; ?></td>
              <td><input type="text" id="date" name="date" value="<?php echo $date; ?>" size="100" readonly="readonly" />
                <?php if ($error_date) { ?>
                <span class="error"><?php echo $error_date; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: right;">
               <a onclick="$('.checkboxclass').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$('.checkboxclass').attr('checked', false);"><?php echo 'UnSelect All'; ?></a>
              </td>
            </tr>
            <tr>
              <td style="border-bottom: 2px solid;"><?php echo "Division"; ?></td>
              <td style="border-bottom: 2px solid;">
                <div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($division_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php /* if (in_array($dkey, $holi_datas[$ukey])) { */ ?>
                    <?php if (array_key_exists($dkey, $holi_division_datas)) { ?>
                    <input type="checkbox" name="holi_division_datas[<?php echo $dkey; ?>]" value="<?php echo $dvalue['name']; ?>" class="checkboxclass division-check" id="division-check_<?php echo $dvalue['id'] ?>" checked="checked" />
                    <?php echo $dvalue['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="holi_division_datas[<?php echo $dkey; ?>]" value="<?php echo $dvalue['name']; ?>" class="checkboxclass division-check" id="division-check_<?php echo $dvalue['id'] ?>" />
                    <?php echo $dvalue['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
              </td>
            </tr>
            <?php if ($unit_data) { ?>
              <?php foreach ($unit_data as $ukey => $unit) { ?>
                <tr class="unit_tr tr_<?php echo $unit['division_id'] ?>">
                  <td colspan="2">
                    <?php echo "Site( ".$unit['name'].") - " . $unit['division_name']; ?> : 
                    <div class="scrollbox" style="position:absolute;left:-999px;">
                      <?php $class = 'even'; ?>
                      <?php foreach ($department_data as $dkey => $dvalue) { ?>
                      <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                      <div class="<?php echo $class; ?>">
                        <?php /* if (in_array($dkey, $holi_datas[$ukey])) { */ ?>
                        <?php if (array_key_exists($dkey, $holi_datas[$ukey])) { ?>
                        <input class="checkboxclass" type="checkbox" name="holi_datas[<?php echo $ukey; ?>][<?php echo $dkey; ?>]" value="<?php echo $dvalue; ?>" checked="checked" />
                        <?php //echo $dvalue; ?>
                        <?php } else { ?>
                        <input class="checkboxclass" type="checkbox" name="holi_datas[<?php echo $ukey; ?>][<?php echo $dkey; ?>]" value="<?php echo $dvalue; ?>" />
                        <?php //echo $dvalue; ?>
                        <?php } ?>
                      </div>
                      <?php } ?>
                    </div>
                    <?php if($unit['selected'] == '1'){ ?>
                      <input onclick="return false;" readonly="readonly" class="checkboxclass" type="checkbox" value="1" checked="checked" />
                    <?php } else { ?>
                      <input onclick="return false;" readonly="readonly" class="checkboxclass" type="checkbox" value="1" />
                    <?php } ?>
                    <a class="sel_<?php echo $unit['division_id'] ?>" onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select'; ?></a> / <a class="unsel_<?php echo $unit['division_id'] ?>" onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'UnSelect'; ?></a>
                  </td>
                </tr>
              <?php } ?>
            <?php } ?>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 

$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#date').datepicker({dateFormat: 'yy-mm-dd'});
});

$(".division-check").change(function() {
  idss = $(this).attr('id');
  s_id = idss.split('_');
  id = s_id[1];

  //$('.unit_tr').hide();
  var ischecked = $(this).is(':checked');
  if(!ischecked){
    $('.tr_'+id).hide();
    //$('.unsel_'+id).trigger('click');
  } else {
    $('.tr_'+id).show();
    //$('.sel_'+id).trigger('click');
  }
});

$(document).ready(function() {
  var res = $("input[class^='checkboxclass division-check']:checked").map(function() {
    idss = $(this).attr('id');
    s_id = idss.split('_');
    id = s_id[1];
    return id;
  }).get();
  $('.unit_tr').hide();
  for(i=0; i<res.length; i++){
    $('.tr_'+res[i]).show();
  }
  //console.log(res);
}); 

//--></script>
<?php echo $footer; ?>