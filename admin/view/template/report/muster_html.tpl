<?php date_default_timezone_set("Asia/Kolkata"); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;font-weight: bold;color: #000;">
    <?php echo $filter_company; ?><br />
    <?php echo $title; ?><br />
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'For Month : '. $month_of; ?><br />
    </span>
    <span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
      <?php echo 'Contractor : '. $filter_contractor; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
    </span>
    <span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  </h1>
  <table class="product" style="width: 100%;">
    <?php $count = 0; ?>
    <?php if($final_datas) { ?>
      <thead>
        <tr>
          <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Sr.No</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Employee Id</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Employee Name</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Category</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Site</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Region</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Division</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Department</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('16', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Designation</td>
          <?php } ?>
          <?php foreach($days as $dkey => $dvalue) { ?>
            <td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $dvalue['day_name']; ?></td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('9', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Present</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('10', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Absent</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('11', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Leave</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('12', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Holiday</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('13', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Weekly Off</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('14', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Total Present Days</td>
          <?php } ?>
          <?php if(empty($filter_fields) || in_array('15', $filter_fields) ){ ?>
            <?php $count ++; ?>
            <td style="padding: 0px 9px;font-size: 11px;">Total Month Days</td>
          <?php } ?>
        </tr>
      </thead>
    <?php } ?>
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas['contractor_datas'] as $final_dat) { ?>
          <tr>
            <td colspan = "<?php echo count($final_dat['employee_datas'][0]['transaction_datas']) + $count; ?>" style="font-weight: bold;">
              <?php echo 'Contractor : ' . $final_dat['contractor']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Contractor Code : '.$final_dat['contractor_code']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Total Present Count : '.$final_dat['total_present_count']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Total Absent Count : '.$final_dat['total_absent_count']; ?>
            </td>
          </tr>
          <?php $i = 1; ?>
          <?php foreach($final_dat['employee_datas'] as $ekey => $final_data) { ?>
            <tr>
              <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $i; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['emp_code']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['name']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['category']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['region']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['division']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['unit']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['department']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('16', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['designation']; ?></td>
              <?php } ?>
              <?php foreach($final_data['transaction_datas'] as $tkey => $tvalue) { ?>
                <td style="padding: 0px 9px;font-size:11px;">
                  <?php echo $tvalue['status']; ?>
                </td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('9', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['present_count']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('10', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['absent_count']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('11', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['leave_count']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('12', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['holiday_count']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('13', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['week_count']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('14', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['tpresent_count']; ?></td>
              <?php } ?>
              <?php if(empty($filter_fields) || in_array('15', $filter_fields) ){ ?>
                <td style="padding: 0px 9px;"><?php echo $final_data['total_cnt']; ?></td>
              <?php } ?>
            </tr>
            <?php $i++; ?>
          <?php } ?>
          <tr style="border-bottom:2px solid black;display: none;">
            <td colspan = "<?php echo count($final_dat['employee_datas'][0]['transaction_datas']) + $count; ?>" >
            </td>
          </tr>
        <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div></body></html>