<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
        </div>
        <div class="content sales-report">
            <table class="form">
                <tr>
                    <td style="width:9%;"><?php echo "Date"; ?>
                        <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" />
                    </td>
                    <td style="width:9%">Location
                        <select name="filter_unit" id="filter_unit" style="width: 110px;">
                            <?php foreach($location_datas as $key => $ud) { ?>
                                <?php if($ud['location_name'] == $filter_unit) { ?>
                                    <option value="<?php echo $ud['location_name']; ?>" selected="selected"><?php echo $ud['location_name']; ?></option> 
                                <?php } else { ?>
                                    <option value="<?php echo $ud['location_name']; ?>"><?php echo $ud['location_name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                    <td style="text-align: right;">
                        <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
                        <a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
                    </td>
                </tr>
            </table>
            <?php if($final_datas){ ?>
                <table class="list">
                    <thead>
                        <tr>
                            <td class="center" style="padding: 10px;font-size: 20px;font-weight: bold;"><?php echo 'Sr.No'; ?></td>
                            <td class="center" style="padding: 10px;font-size: 20px;font-weight: bold;"><?php echo 'Location'; ?></td>
                            <td class="center" style="padding: 10px;font-size: 20px;font-weight: bold;" colspan='3'><?php echo 'Shift'; ?></td>
                        </tr>
                        <tr>
                            <td class="center" style="padding: 10px;font-size:15px;">&nbsp;</td>
                            <td class="center" style="padding: 10px;font-size:15px;">&nbsp;</td>
                            <?php foreach($final_datas[0]['assign_shift_datas'] as $fkeyss => $fvaluess){ ?>
                                <td class="center" style="padding: 10px;font-size:15px;font-weight:bold;"><?php echo $fkeyss; ?></td>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $cnt = 1; ?>
                        <?php foreach($final_datas as $fkeys => $fvalues) { ?>
                            <tr>
                                <td class="center" style="font-size:15px;"><?php echo $cnt; ?></td>
                                <td class="center" style="font-size:15px;"><?php echo $fvalues['location']; ?></td>
                                <?php foreach($fvalues['assign_shift_datas'] as $fkey => $fvalue){ ?>
                                    <td class="center" style="padding: 10px;"><?php echo $fvalue; ?></td>
                                <?php } ?>
                            </tr> 
                            <?php $cnt ++; ?>
                        <?php } ?>
                        <tr>
                            <td class="center" style="border-top: 2px solid #000;border-bottom: 2px solid #000;padding: 10px;font-size:20px;font-weight:bold;" colspan="2"><?php echo 'Total'; ?></td>
                            <?php foreach($total_shift_datas_assign as $tkey => $tvalue){ ?>
                                <td class="center" style="border-top: 2px solid #000;border-bottom: 2px solid #000;padding: 10px;font-size:20px;font-weight:bold;"><?php echo $tvalue; ?></td>
                            <?php } ?>
                        </tr> 
                    </tbody>
                </table>
            <?php } else { ?>
                <tr style="">
                    <td class="center" colspan="3"><?php echo 'No Data Found'; ?></td>
                </tr>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
function filter(val) {
  url = 'index.php?route=report/attendance2&token=<?php echo $token; ?>';
    
    var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
    if (filter_date_start) {
      url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
    }

    var filter_unit = $('select[name=\'filter_unit\']').attr('value');
    if (filter_unit && filter_unit != 'All') {
      url += '&filter_unit=' + encodeURIComponent(filter_unit);
    }

    url += '&once=1';
    
    location = url;
    return false;
}
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
});
//--></script>
<?php echo $footer; ?>