<?php 
date_default_timezone_set("Asia/Kolkata");
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
	<h1 style="text-align:center;font-weight: bold;color: #000;">
		<?php echo $filter_company; ?><br />
		<?php echo $title; ?><br />
		<span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
			<?php echo 'For Month : '. $month_of; ?><br />
		</span>
		<span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
			<?php echo 'Contractor : '. $filter_contractor; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Region : '. $filter_region; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_unit; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Department : '. $filter_department; ?><br />
		</span>
		<span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
	</h1>
	<table class="product" style="width: 100%;">
		<?php $count = 0; ?>
		<?php if($final_datas) { ?>
			<thead>
				<tr>
					<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;">Sr.No</td>
					<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;">Contractor Name</td>
					<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;">Contractor Code</td>
					<?php $count = 3; ?>
					<?php foreach($designation_datas as $dkey => $dvalue){ ?>
						<td style="padding: 0px 9px;font-size: 12pxfont-weight: bold;"><?php echo $dvalue; ?></td>
						<?php $count ++ ?>
					<?php } ?>
					<td style="padding: 0px 9px;font-size: 12px;text-align: right;font-weight: bold;">Total</td>
					<?php $count ++ ?>
				</tr>
			</thead>
		<?php } ?>
		<tbody>
			<?php if($final_datas) { ?>
				<?php foreach($final_datas['category_datas'] as $fkeys => $final_data) { ?>
					<?php $i = 1; ?>
					<tr>
						<td colspan = "<?php echo $count; ?> " style="font-weight: bold;">
							<?php echo 'Category : ' . $final_data['category_name']; ?>
						</td>
					</tr>
					<?php foreach($final_data['contractor_datas'] as $ckeys => $cvalues){ ?>
						<tr>
							<td style="padding: 0px 9px;font-size: 10px;text-align: left;"><?php echo $i; ?></td>
							<td style="padding: 0px 9px;font-size: 10px"><?php echo $cvalues['contractor_name']; ?></td>
							<td style="padding: 0px 9px;font-size: 10px;text-align: right;"><?php echo $cvalues['contractor_code']; ?></td>
							<?php foreach($cvalues['designation_datas'] as $dkeys => $dvalues){ ?>
								<td style="padding: 0px 9px;font-size: 10px;"><?php echo $dvalues ?></td>
							<?php } ?>
							<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;"><?php echo $cvalues['contractor_present_count']; ?></td>
						</tr>
						<?php $i++; ?>
					<?php } ?>
					<tr>
						<td colspan = "3" style="font-weight: bold;">
							<?php echo 'Sub-Total : '; ?>
						</td>
						<?php foreach($final_data['sub_designation_datas'] as $sdkeys => $sdvalues){ ?>
							<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;"><?php echo $sdvalues ?></td>
						<?php } ?>
						<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;"><?php echo $final_data['sub_total_contractor_present_count']; ?></td>
					</tr>
				<?php } ?>
				<tr>
					<td colspan = "3" style="font-weight: bold;">
						<?php echo 'Total : '; ?>
					</td>
					<?php foreach($final_datas['total_designation_datas'] as $tdkeys => $tdvalues){ ?>
						<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;"><?php echo $tdvalues ?></td>
					<?php } ?>
					<td style="padding: 0px 9px;font-size: 12px;font-weight: bold;"><?php echo $final_datas['total_contractor_present_count']; ?></td>
				</tr>
			<?php } else { ?>
				<tr>
					<td class="center" colspan = "<?php echo $count; ?> "><?php echo $text_no_results; ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
</div></body></html>