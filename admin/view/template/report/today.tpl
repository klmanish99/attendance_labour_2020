<?php echo $header; ?>
<?php
$route = '';
if (isset($this->request->get['route'])) {
	$part = explode('/', $this->request->get['route']);

	if (isset($part[0])) {
		$route .= $part[0];
	}

	if (isset($part[1])) {
		$route .= '/' . $part[1];
	}
}
?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/report.png" alt="" /> <?php echo "Today's Attendance Report"; ?></h1>
		</div>
		<div class="content sales-report">
			<table class="form">
				<tr>
					<td style="width:10%;"><?php echo "Name / Emp Code"; ?>
						<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="22" />
						<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
					</td>
					<td style="width:6%">Company
						<select multiple name="filter_company[]" id="input-filter_company" style="display:inline;">
							<?php foreach($company_data as $key => $ud) { ?>
								<?php if (in_array($key, $filter_company)) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%;display: none;">Region
						<select name="filter_region" id="filter_region">
							<?php foreach($region_data as $key => $ud) { ?>
								<?php if($key == $filter_region) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Division
						<select name="filter_division" id="filter_division">
							<?php foreach($division_data as $key => $ud) { ?>
								<?php if($key == $filter_division) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Site
						<select name="filter_unit" id="filter_unit">
							<?php foreach($unit_data as $key => $ud) { ?>
								<?php if($key == $filter_unit) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Contractor
						<select multiple name="filter_contractor[]" id="input-filter_contractor" style="display:inline;">
							<?php foreach($contractor_data as $key => $ud) { ?>
								<?php if (in_array($key, $filter_contractor)) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="width:6%">Department
						<select name="filter_department" id="filter_department" style="width:100%;">
							<?php foreach($department_data as $key => $dd) { ?>
								<?php if($key == $filter_department) { ?>
									<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
								<?php } else { ?>
									<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</td>
					<td style="text-align: right;">
						<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
						<?php if($this->user->hasPermission('modify', $route)){ ?>
							<a style="padding: 13px 25px;display: none;" href="<?php echo $generate_today; ?>" class="button"><?php echo "Refresh"; ?></a>
						<?php } ?>
						<?php if($this->user->hasPermission('modify', $route)){ ?>
							<a style="padding: 13px 25px;" href="<?php echo $export; ?>" class="button"><?php echo "Export"; ?></a>
						<?php } ?>
					</td>
				</tr>
			</table>
			<div style="width: 1459px;overflow-x: scroll;overflow-y: hidden;">
				<table class="list" style="width: 100%;">
					<thead>
						<tr>
							<td class="center"><?php echo 'Sr.No'; ?></td>
							<td class="center"><?php echo 'Emp Code'; ?></td>
							<td class="center"><?php echo 'Name'; ?></td>
							<td class="center"><?php echo 'Shift Assigned'; ?></td>
							<td class="center"><?php echo 'Shift Attended'; ?></td>
							<td class="center"><?php echo 'Late time'; ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td class="center">In</td>
							<?php /* ?>
							<td class="center">Out</td>
							<?php */ ?>
							<td class="center">In</td>
							<?php /* ?>
							<td class="center">Out</td>
							<?php */ ?>
							<td>&nbsp;</td>
							
						</tr>
					</thead>
					<tbody>
						<?php if($results) { ?>
							<?php $i = 1; ?>
							<?php foreach($results as $result) { ?>
								<tr>
									<td>
										<?php echo $i; ?>
									</td>
									<td>
										<?php echo $result['emp_id']; ?>
									</td>
									<td>
										<?php echo $result['emp_name']; ?>
									</td>
									<td>
										<?php echo $result['shift_intime']; ?>
									</td>
									<td>
										<?php echo $result['act_intime']; ?>
									</td>
									<td>
										<?php if($result['late_time'] != '00:00:00') { ?>
											<span style="background-color:red;">
												<?php echo $result['late_time']; ?>
											</span>
										<?php } else { ?>
											<?php echo $result['late_time']; ?>
										<?php } ?>
									</td>
								</tr>
								<?php $i++; ?>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="6"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=report/today&token=<?php echo $token; ?>';
	
	var filter_name = $('#filter_name').val();
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
		var filter_name_id = $('#filter_name_id').val();
		if (filter_name_id) {
			url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
		} else {
			alert('Please Enter Correct Employee Name');
			return false;
		}
	}

	var filter_unit = $('#filter_unit').val();
	if (filter_unit && filter_unit != '0') {
		url += '&filter_unit=' + encodeURIComponent(filter_unit);
	}

	var filter_department = $('#filter_department').val();
	if (filter_department && filter_department != '0') {
		url += '&filter_department=' + encodeURIComponent(filter_department);
	}

	var filter_division = $('#filter_division').val();
	if (filter_division && filter_division != '0') {
		url += '&filter_division=' + encodeURIComponent(filter_division);
	}

	var filter_region = $('#filter_region').val();
	if (filter_region && filter_region != '0') {
		url += '&filter_region=' + encodeURIComponent(filter_region);
	}

	company_selected = $('#input-filter_company').val();
	field_ids = '';
	if(company_selected){
		max_length = company_selected.length - 1;
		for(i=0; i<company_selected.length; i++){
			if(max_length == i){  
				field_ids += company_selected[i];
			} else {
				field_ids += company_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_company=' + encodeURIComponent(field_ids);
		}
	}

	contractor_selected = $('#input-filter_contractor').val();
	field_ids = '';
	if(contractor_selected){
		max_length = contractor_selected.length - 1;
		for(i=0; i<contractor_selected.length; i++){
			if(max_length == i){  
				field_ids += contractor_selected[i];
			} else {
				field_ids += contractor_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_contractor=' + encodeURIComponent(field_ids);
		}
	}

	filter_fields_selected = $('#input-filter_fields').val();
	field_ids = '';
	if(filter_fields_selected){
		max_length = filter_fields_selected.length - 1;
		for(i=0; i<filter_fields_selected.length; i++){
			if(max_length == i){  
				field_ids += filter_fields_selected[i];
			} else {
				field_ids += filter_fields_selected[i]+',';
			}
		}
		if (field_ids) {
			url += '&filter_fields=' + encodeURIComponent(field_ids);
		}
	} 
	
	location = url;
	return false;
}

$(document).ready(function() {
	$('#input-filter_company, #input-filter_contractor').multiselect({
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		maxHeight: 235,
	});
});

$('input[name=\'filter_name\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=report/daily/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {   
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.emp_code
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
		$('input[name=\'filter_name_id\']').val(ui.item.value);
		return false;
	},
	focus: function(event, ui) {
		return false;
	}
});

$('#filter_region').on('change', function() {
	filter_region = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(filter_region),
		dataType: 'json',
		success: function(json) {   
			$('#filter_division').find('option').remove();
			if(json['division_datas']){
				$.each(json['division_datas'], function (i, item) {
					$('#filter_division').append($('<option>', { 
						value: item.division_id,
						text : item.division 
					}));
				});
			}
			$('#unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#filter_unit').append($('<option>', { 
						value: item.unit_id,
						text : item.unit 
					}));
				});
			}
		}
	});
});

$('#filter_division').on('change', function() {
	filter_division = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(filter_division),
		dataType: 'json',
		success: function(json) {   
			$('#filter_unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#filter_unit').append($('<option>', { 
						value: item.unit_id,
						text : item.unit 
					}));
				});
			}
		}
	});
});

$('#filter_unit').on('change', function() {
	filter_unit = $(this).val();
	$.ajax({
		url: 'index.php?route=catalog/employee/getcontractors_by_unit_report&token=<?php echo $token; ?>&filter_unit_id=' +  encodeURIComponent(filter_unit),
		dataType: 'json',
		success: function(json) {   
			$("#input-filter_contractor").multiselect("destroy");
			$('#input-filter_contractor').find('option').remove();
			if(json['contractor_datas']){
				$.each(json['contractor_datas'], function (i, item) {
					// $('#input-filter_contractor').multiSelect('addOption', { value: 42, text: 'test 42', index: 0 });
					$('#input-filter_contractor').append($('<option>', { 
						value: item.contractor_id,
						text : item.contractor_name 
					}));
				});
			}
			$("#input-filter_contractor").multiselect({
				includeSelectAllOption: true,
				enableCaseInsensitiveFiltering : true,
				enableFiltering: true,
				maxHeight: 235,
			});
		}
	});
});

/*
$('#input-filter_contractor').on('change', function() {
	contractor_selected = $(this).val();
	field_ids = '';
	if(contractor_selected){
		max_length = contractor_selected.length - 1;
		for(i=0; i<contractor_selected.length; i++){
			if(max_length == i){  
				field_ids += contractor_selected[i];
			} else {
				field_ids += contractor_selected[i]+',';
			}
		}
	}
	filter_contractor = field_ids;
	$.ajax({
		url: 'index.php?route=catalog/employee/getlocation_by_contractor&token=<?php echo $token; ?>&filter_contractor_id=' +  encodeURIComponent(filter_contractor),
		dataType: 'json',
		success: function(json) {   
			$('#filter_unit').find('option').remove();
			if(json['unit_datas']){
				$.each(json['unit_datas'], function (i, item) {
					$('#filter_unit').append($('<option>', { 
						value: item.unit_id,
						text : item.unit 
					}));
				});
			}
		}
	});
});
*/
//--></script> 
<?php echo $footer; ?>