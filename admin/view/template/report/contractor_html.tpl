<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  	<h1 style="text-align:center;font-weight: bold;color: #000;">
		<?php echo $title; ?><br />
		<span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
	  		<?php echo 'Period : '. $date_start.' - '.$date_end; ?><br />
		</span>
		<span style="display:inline;font-size:15px;font-weight: bold;color: #000;">
	  		<?php echo 'Division : '. $filter_division; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Site : '. $filter_location; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Contractor : '. $filter_contractor; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Labour Category : '. $filter_category; ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php echo 'Designation : '. $filter_designation; ?><br />
		</span>
		<span style="display:inline;font-size:15px;float: right;font-weight: bold;color: #000;"><?php echo 'Generate On : '. Date('d-F-Y h:i:s A'); ?></span>
  	</h1>
  	<table class="product" style="width:100% !important;">
	<?php $count = 0; ?>
	<?php if($final_datas) { ?>
	  	<thead>
			<tr>
	            <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Division</td>
	            <?php } ?>
	            <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Site</td>
	            <?php } ?>
	            <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Contractor Code</td>
	            <?php } ?>
	            <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Contractor Name</td>
	            <?php } ?>
	            <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Date</td>
	            <?php } ?>
	            <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Labour Category</td>
	            <?php } ?>
	            <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Designation</td>
	            <?php } ?>
	             <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
	                <?php $count ++; ?>
	                <td style="padding: 0px 9px;font-size: 11px;font-weight: bold;">Total Present Count</td>
	            <?php } ?>
            </tr>
	  	</thead>
  		<tbody>
			<?php $i = 1; ?>
			<?php foreach($final_datas as $fkey => $final_data){ ?>
				<tr>
                    <?php if(empty($filter_fields) || in_array('1', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['division'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('2', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['location'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('3', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['contractor_code'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('4', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['contractor_name'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('5', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['date'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('6', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['category_name'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('7', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['designation'] ?></td>
                    <?php } ?>
                    <?php if(empty($filter_fields) || in_array('8', $filter_fields) ){ ?>
                      	<td style="padding: 0px 9px;font-size: 10px"><?php echo $final_data['total_count'] ?></td>
                    <?php } ?>
              	</tr>
			<?php $i++; ?>
			<?php } ?>
  		</tbody>
	<?php } else { ?>
		<tr>
	  		<td class="center" colspan = "<?php echo $count; ?> "><?php echo $text_no_results; ?></td>
		</tr>
	<?php } ?>
  	</table>
</div></body></html>