<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $title; ?></title>
		<base href="<?php echo $base; ?>" />
		<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
	</head>
	<body>
	  	<div style="page-break-after: always;">
			<h1 style="text-align:center;">
		 	 	<?php //echo 'Econ Shipping PVT LTD.'; ?><br />
		  		<?php echo $title; ?><br />
		  		<p style="font-size:15px;"><?php echo 'Date : '. $filter_date_start . ' &nbsp;&nbsp;&nbsp;&nbsp; Location : ' . $filter_unit; ?></p>
		  		<p style="display:inline;font-size:15px;float: right"><?php echo 'Generated On : '. Date('d-M-Y'); ?></p>
			</h1>
			<?php if($final_datas){ ?>
	            <table class="product" style="width: 100%;">
	                <thead>
	                    <tr>
	                        <td class="center" style="padding: 10px;font-size: 20px;font-weight: bold;"><?php echo 'Sr.No'; ?></td>
	                        <td class="center" style="padding: 10px;font-size: 20px;font-weight: bold;"><?php echo 'Location'; ?></td>
	                        <td class="center" style="padding: 10px;font-size: 20px;font-weight: bold;" colspan='3'><?php echo 'Shift'; ?></td>
	                    </tr>
	                    <tr>
	                        <td class="center" style="padding: 10px;font-size:15px;">&nbsp;</td>
	                        <td class="center" style="padding: 10px;font-size:15px;">&nbsp;</td>
	                        <?php foreach($final_datas[0]['assign_shift_datas'] as $fkeyss => $fvaluess){ ?>
	                            <td class="center" style="padding: 10px;font-size:15px;font-weight:bold;"><?php echo $fkeyss; ?></td>
	                        <?php } ?>
	                    </tr>
	                </thead>
	                <tbody>
	                    <?php $cnt = 1; ?>
	                    <?php foreach($final_datas as $fkeys => $fvalues) { ?>
	                        <tr>
	                            <td class="center" style="font-size:15px;"><?php echo $cnt; ?></td>
	                            <td class="center" style="font-size:15px;"><?php echo $fvalues['location']; ?></td>
	                            <?php foreach($fvalues['assign_shift_datas'] as $fkey => $fvalue){ ?>
	                                <td class="center" style="padding: 10px;"><?php echo $fvalue; ?></td>
	                            <?php } ?>
	                        </tr> 
	                        <?php $cnt ++; ?>
	                    <?php } ?>
	                    <tr>
	                        <td class="center" style="border-top: 2px solid #000;border-bottom: 2px solid #000;padding: 10px;font-size:20px;font-weight:bold;" colspan="2"><?php echo 'Total'; ?></td>
	                        <?php foreach($total_shift_datas_assign as $tkey => $tvalue){ ?>
	                            <td class="center" style="border-top: 2px solid #000;border-bottom: 2px solid #000;padding: 10px;font-size:20px;font-weight:bold;"><?php echo $tvalue; ?></td>
	                        <?php } ?>
	                    </tr> 
	                </tbody>
	            </table>
	        <?php } else { ?>
	            <tr style="">
	                <td class="center" colspan="3"><?php echo 'No Data Found'; ?></td>
	            </tr>
	        <?php } ?>
	  	</div>
	</body>
</html>